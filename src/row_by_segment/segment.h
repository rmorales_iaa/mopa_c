/*
 * segement.h
 *
 *  Created on: 26 Nov 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef ROW_BY_SEGMENT_SEGMENT_H_
#define ROW_BY_SEGMENT_SEGMENT_H_
//=============================================================================
#include <sys/types.h>
//=============================================================================
class Segment{
  //---------------------------------------------------------------------------
  private:
    uint start;
    uint end;
  //---------------------------------------------------------------------------
  public :
    //-------------------------------------------------------------------------
    Segment(uint _start, uint _end){start = _start; end = _end;}
    //-------------------------------------------------------------------------
    virtual ~Segment(){};
    //-------------------------------------------------------------------------
    uint get_start(void){return start;}
    //-------------------------------------------------------------------------
    uint get_end(void){return end;}
    //-------------------------------------------------------------------------
    bool is_in(uint x){ return (x >= start) && (x <= end); }
    //-------------------------------------------------------------------------
    bool exist_intersection(Segment &s){return is_in(s.start) || is_in(s.end); }
    //-------------------------------------------------------------------------
    bool is_sub_segment(Segment &s){ return is_in(s.start) && is_in(s.end); }
    //-------------------------------------------------------------------------
    Segment get_intersection(Segment &s) {

      if (is_sub_segment(s)) return Segment(s.start, s.end);
      else
	if (s.is_sub_segment(*this)) return Segment(start, end);
	else return Segment(max(start, s.start),  min(end, s.end)); //there is at least one point of intersection at the borders
    }
    //-------------------------------------------------------------------------
};

//=============================================================================
#endif /* ROW_BY_SEGMENT_SEGMENT_H_ */
