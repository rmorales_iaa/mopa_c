/*
 * rowbysegment.h
 *
 *  Created on: 26 Nov 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef ROW_BY_SEGMENT_ROW_BY_SEGMENT_H_
#define ROW_BY_SEGMENT_ROW_BY_SEGMENT_H_
//=============================================================================
#include <vector>
#include <map>
//=============================================================================
#include "segment.h"
//=============================================================================
class Row_by_segment;
typedef std::pair<uint, Row_by_segment> Row_pair;
typedef std::map <uint, Row_by_segment> Row_map;
//=============================================================================
extern uint x_pix_max_index;
extern uint y_pix_max_index;
//=============================================================================
class Row_by_segment {
	//---------------------------------------------------------------------
  private :
    uint row_index;
    std::vector<Segment> storage;
  public :
	//---------------------------------------------------------------------
    Row_by_segment() {row_index = -1;}
    Row_by_segment(uint _row_index){row_index = _row_index;}
    //-------------------------------------------------------------------------
    Row_by_segment(uint _row_index,uint start, uint end){
      row_index = _row_index;
      storage.push_back(Segment(start, end));
    }
    //-------------------------------------------------------------------------
    virtual ~Row_by_segment(){};
    //-------------------------------------------------------------------------
    uint get_row_index(void){return row_index;}
    //-------------------------------------------------------------------------
    std::vector<Segment>& get_storage(void){return storage;}
    //-------------------------------------------------------------------------
    void add_segment(uint start, uint end) { storage.push_back(Segment(start, end));}
    //-------------------------------------------------------------------------
    uint get_segment_count(void){return storage.size();}
    //-------------------------------------------------------------------------
    Segment get_segment(uint i){return storage[i];}
    //-------------------------------------------------------------------------
    void clear(void){storage.clear();}
    //-------------------------------------------------------------------------
    void add( std::vector<Segment> v){
      for(auto& s: v)
	storage.push_back(s);
    }
    //-------------------------------------------------------------------------
    bool is_in(uint x){
      for(auto& s: storage)
        if (s.is_in(x)) return true;
      return false;
    }
    //-------------------------------------------------------------------------
    bool get_limits(uint& x_min, uint& x_max){

      uint x_min_mask = x_pix_max_index;
      uint x_max_mask = 0;

      uint xs=0;
      uint xe=0;

      for(auto& s: storage){
        xs = s.get_start();
        xe = s.get_end();
        if (xs < x_min_mask) x_min_mask = xs;
        if (xe > x_max_mask) x_max_mask = xe;
      }

      x_min =  xs;
      x_max =  xe;

      return false;
    }
    //-------------------------------------------------------------------------
};
//=============================================================================
#endif /* ROW_BY_SEGMENT_ROW_BY_SEGMENT_H_ */
