/*
 * blob.h
 *
 *  Created on: Dec 30, 2019
 *      Author: rafa
 */
//============================================================================
#ifndef BLOB_DETECTION_H_
#define BLOB_DETECTION_H_
//============================================================================
#include <vector>
//------------------------------------------------------------------------
#include <opencv2/core/types.hpp>
//------------------------------------------------------------------------
#include "type_definition.h"
#include "logger/minilog.h"
#include "global_var.h"
#include "segment2d.h"
//============================================================================
typedef std::vector<Segment2d> Blob_type;
//============================================================================
static random_device blob_my_random_device;
static mt19937_64 blob_my_random_engine(blob_my_random_device());
static uniform_int_distribution<int> blob_color_random_dist(0, 255);
//============================================================================
class Blob
{
  private:
  //--------------------------------------------------------------------------
  std::vector<Blob_type> blob_storage;
  uint blob_max_allowed_x_distance;
  uint blob_max_allowed_y_distance;
  //--------------------------------------------------------------------------
  void stack_position(uint x, uint y){

    Segment2d new_segment(x,y);
    for(auto & blob: blob_storage){

      //try to stack to same row
      for (auto it = blob.rbegin(); it != blob.rend(); ++it)
       if(it->try_to_add(x, y ,Global_var::user_blob_max_allowed_x_distance)) return;  //try to add to current last_segment

      //try to stack to different row
      for (auto it = blob.rbegin(); it != blob.rend(); ++it){
        if (abs(y - it->y) > Global_var::user_blob_max_allowed_y_distance) break;
          if (it->is_in(x, Global_var::user_blob_max_allowed_x_distance)) {
            blob.push_back(new_segment);
            return;
          }
      }
    }
    //add to storage
    Blob_type new_segment_vector;
    new_segment_vector.push_back(new_segment);
    blob_storage.push_back(new_segment_vector);
  }
  //------------------------------------------------------------------------
  static void get_blob_max_pix_size(Blob_type& blob, uint& x_size, uint& y_size) {

    y_size = blob.size();
    x_size = 0;
    uint segment_size = 0;
    for(auto& segment: blob){
      segment_size = segment.max_x - segment.min_x;
      if (segment_size > x_size) x_size = segment_size;
    }
  }
  //------------------------------------------------------------------------
  static void get_blob_pix_count(Blob_type& blob, uint& pix_count) {

    pix_count = 0;
    for(auto& segment: blob)
      pix_count += (segment.max_x - segment.min_x);
  }
  //--------------------------------------------------------------------------
  public:
  //------------------------------------------------------------------------
    Blob() {
      blob_max_allowed_x_distance = 0;
      blob_max_allowed_y_distance = 0;
    }
    //------------------------------------------------------------------------
    Blob(FrameDataType& frame, uint _blob_max_allowed_x_distance, uint _blob_max_allowed_y_distance) {

      blob_max_allowed_x_distance = _blob_max_allowed_x_distance;
      blob_max_allowed_y_distance = _blob_max_allowed_y_distance;

      uchar r;
      uchar g;
      uchar b;
      cv::Vec3b pix;
      My_point2D p;

      //find all segments row by row
      for(int y=0; y < frame.rows; y++){
        for(int x=0; x < frame.cols; x++){
          pix = frame.at<cv::Vec3b>(y,x);
          b = pix[0];
          g = pix[1];
          r = pix[2];

          if(((r >= Global_var::user_blob_min_r) && (r <= Global_var::user_blob_max_r)) &&
             ((g >= Global_var::user_blob_min_g) && (g <= Global_var::user_blob_max_g)) &&
             ((b >= Global_var::user_blob_min_b) && (b <= Global_var::user_blob_max_b)))
               stack_position(x, y);
        }
      }
    }
    //------------------------------------------------------------------------
    virtual ~Blob(){};
    //------------------------------------------------------------------------
    ulong get_blob_count(void){return blob_storage.size();}
    //------------------------------------------------------------------------
    void filter_blob(uint min_allowed_pix_size) {

      uint current_pix_count;
      uint blob_count = blob_storage.size();
      std::vector<Blob_type> blob_storage_filtered;
      if (blob_count == 0) return;

      for(uint i=blob_count -1;; --i){
        Blob_type blob = blob_storage[i];
        get_blob_pix_count(blob, current_pix_count);
	if (current_pix_count <= min_allowed_pix_size) blob_storage.erase(blob_storage.begin() + i);
	if (i==0) break;
      }
    }
    //------------------------------------------------------------------------
    bool is_position_inside(uint x, uint y) {
      for(auto & blob: blob_storage)
        for(auto& segment: blob)
          if((segment.y == y) && (x >= segment.min_x) && (x <= segment.max_x)) return true;
      return false;
    }
    //------------------------------------------------------------------------
    void get_blob_mask(uint row_count, uint col_count, FrameDataType& frame_mask){

      uint x;
      uint y;
      cv::Vec3b pix;
      blob_color_random_dist( blob_my_random_engine );

      for(auto & blob: blob_storage){
        pix[0] = blob_color_random_dist( blob_my_random_engine );
	pix[1] = blob_color_random_dist( blob_my_random_engine );
	pix[2] = blob_color_random_dist( blob_my_random_engine );
        for(auto& segment: blob){
          y = segment.y;
          for(x=segment.min_x; x<=segment.max_x;++x)
            frame_mask.at<cv::Vec3b>(y,x) = pix;
        }
      }
    }
    //------------------------------------------------------------------------
    void print(void){

      uint blob_index = 0;
      uint pix_count = 0;
      MINILOG(logINFO) << "-------------------";
      for (auto& blob : blob_storage){
	get_blob_pix_count(blob, pix_count);
	MINILOG(logINFO) << ".........Blob: " << blob_index++ << " starts. pix_size: " << pix_count << "..............";
        for(auto& segment: blob)
          MINILOG(logINFO) << "	Segment " <<  " : " << segment.y << "->(" << segment.min_x << "," << segment.max_x << ")";
        MINILOG(logINFO) << ".........Blob: " << blob_index++ << " ends...............";
      }
      MINILOG(logINFO) << "===================";
    }
    //------------------------------------------------------------------------
};

//============================================================================
#endif /* BLOB__DETECTION_H_ */
