/*
 * segment2d.h
 *
 *  Created on: Jan 6, 2020
 *      Author: rafa
 */
//============================================================================
#ifndef BLOB_SEGMENT2D_H_
#define BLOB_SEGMENT2D_H_
//============================================================================
#include "type_definition.h"
#include "geometry/point/my_point2d.h"
//============================================================================
class Segment2d
{
  //--------------------------------------------------------------------------
  private:
  //--------------------------------------------------------------------------
  public:
    uint y;

    uint min_x;
    uint max_x;

    uchar flag;
    //------------------------------------------------------------------------
    Segment2d(uint _x, uint _y) : y(_y) {

      min_x = _x;
      max_x = _x;
      flag = 0;
    }
    //------------------------------------------------------------------------
    virtual ~Segment2d(){};
    //------------------------------------------------------------------------
    bool try_to_add(uint p_x, uint p_y, uint max_allowed_x_distance) {

      if (y != p_y) return false;
      if (!is_in(p_x, max_allowed_x_distance)) return false;
      min_x = std::min(p_x, min_x);
      max_x = std::max(p_x, max_x);
      return true;
    }
    //------------------------------------------------------------------------
    bool is_in(uint x, uint max_allowed_distance){

      if ((x >= min_x) && (x <= max_x)) return true;

      uint new_min_x = 0;
      uint new_max_x = max_x + max_allowed_distance;

      if (min_x >=  max_allowed_distance) new_min_x = min_x - max_allowed_distance;
      else new_min_x = 0;
      if ((x >= new_min_x) && (x <= new_max_x)) return true;

      return false;
    }
  //--------------------------------------------------------------------------
};
//============================================================================
#endif /* BLOB_SEGMENT2D_H_ */
