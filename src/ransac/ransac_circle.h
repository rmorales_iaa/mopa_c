/*
 * circle.h
 *
 *  Created on: Dec 15, 2019
 *      Author: rafa
 *      BAsed on: https://github.com/xiapengchng/Ransac-Fit-a-circle
 */
//=============================================================================
#ifndef RANSAC_RANSAC_CIRCLE_H_
#define RANSAC_RANSAC_CIRCLE_H_
//=============================================================================
#include <cmath>
#include <random>
//-----------------------------------------------------------------------------
#include <opencv2/core/types.hpp>
//-----------------------------------------------------------------------------
static random_device my_ransac_random_device;
static mt19937_64 my_ransac_random_engine(my_ransac_random_device()); //Mersenne Twister 19937 (64 bits) engine
//=============================================================================
class Ransac_circle
{
  public:
    //-------------------------------------------------------------------------
    Ransac_circle(uint min_pix_radius
	       , uint max_allowed_distance
	       , uint max_absolute_loop_iteration
	       , uint min_match_percentage_for_valid_circle
	       , cv::Point2d& best_circle_center
	       , float& best_circle_radius
	       , std::vector<cv::Point2d>& point_list) {

      uint max_fit_num = 0;
      uint match_count = 0;
      uint point_count = point_list.size();
      uint idx0;
      uint idx1;
      uint idx2;
      cv::Point2d centre;
      float radius;
      bool is_circle;

      best_circle_center.x = -1;
      best_circle_center.y = -1;
      best_circle_radius = -1;

      uniform_int_distribution<int> ransac_random_dist(0, point_count);

      for (uint i = 0; i < max_absolute_loop_iteration; i++) {

         idx0 = ransac_random_dist(my_ransac_random_engine);
         idx1 = ransac_random_dist(my_ransac_random_engine);
         idx2 = ransac_random_dist(my_ransac_random_engine);

         if (idx0 == idx1) continue;
         if (idx0 == idx2) continue;
         if (idx2 == idx1) continue;

         // create circle from 3 points:
         build_circle(point_list[idx0], point_list[idx1], point_list[idx2], centre, radius, is_circle);
         if ( !is_circle || (radius < min_pix_radius) ) continue;

         match_count = get_match_points_in_circle(centre, radius, max_allowed_distance, point_list);
         if (match_count > max_fit_num) {
           best_circle_radius = radius;
           best_circle_center = centre;

           float match_percentage = (match_count/ (float)point_count) * 100;
           if (match_percentage >= min_match_percentage_for_valid_circle) return;
         }
      }
    }
    //-------------------------------------------------------------------------
    virtual ~Ransac_circle(){};
    //-------------------------------------------------------------------------
  private:
    //-------------------------------------------------------------------------
    inline double get_point_distance(cv::Point2d pt1, cv::Point2d pt2) {
      return sqrt((pt1.x - pt2.x)*(pt1.x - pt2.x) + (pt1.y - pt2.y)*(pt1.y - pt2.y));
    }
    //-------------------------------------------------------------------------
    uint get_match_points_in_circle(cv::Point2d center
				    , double radius
				    , int max_allowed_distance
				    , std::vector<cv::Point2d>& point_list){
      uint num = 0;
      uint point_count = point_list.size();
      for (uint i = 0; i < point_count; i++)
        if (abs(get_point_distance(point_list[i], center) - radius) < max_allowed_distance) num++;

      return num;
    }
    //---------------------------------------------------------------------------
    inline void build_circle(cv::Point2d& p0
                           , cv::Point2d& p1
                           , cv::Point2d& p2
                           , cv::Point2d& centre
                           , float& radius
  		           , bool& b){

      Circle c = Circle(0, p0.x, p0.y, p1.x, p1.y, p2.x, p2.y, b);

      centre.x = c.get_x_centre();
      centre.y = c.get_y_centre();
      radius = c.get_radious();
   }
  //---------------------------------------------------------------------------
};
//=============================================================================
#endif /* RANSAC_RANSAC_CIRCLE_H_ */
