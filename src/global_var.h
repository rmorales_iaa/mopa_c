/*
 * global_var.h
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef GLOBAL_VAR_H_
#define GLOBAL_VAR_H_
//=============================================================================
#include <list>
//=============================================================================
#include "type_definition.h"
#include "geometry/rectangle/rectangle.h"
#include "geometry/triangle/triangle.h"
#include "geometry/circle/circle.h"
#include "geometry/polygon/polygon.h"
//============================================================================
using namespace std;
//=============================================================================
//in order to allow to initialize the constants inside class, use the c++17 dialect
//compile using: Project properties->C/C++ Build->GCC C++ Compiler->Dialect->Other dialect flags-> -std=c++17
class Global_var {
  public:
    //--------------------------------------------------------------------------
    //configuration
    //-------------------------------------------------------------------------
    //user arguments
    static inline string user_output_directoy;

    //search
    static inline bool   user_run_search_command = false;
    static inline string user_search_conf_filename;
    static inline uchar  user_search_frame_max_color_variation_allowed;

    static inline bool   user_mask_on = false;

    static inline uint user_limbo_ransac_min_pix_radius = 200;
    static inline uint user_limbo_ransac_max_allowed_distance = 4;
    static inline uint user_limbo_ransac_min_percentage_for_valid_circle = 80;
    static inline uint user_limbo_ransac_max_loop_iteration = 2000;

    static inline string user_limbo_output_dir;
    static inline string user_flash_cross_match_output_dir = "output/cross_match/";

    //blob
    static inline uchar user_blob_filter_by_activated = 1;
    static inline uchar user_blob_save_frames = 1;

    static inline uchar user_blob_min_r = 0xE0;
    static inline uchar user_blob_min_g = 0xE0;
    static inline uchar user_blob_min_b = 0xE0;

    static inline uchar user_blob_max_r = 0xFF;
    static inline uchar user_blob_max_g = 0xFF;
    static inline uchar user_blob_max_b = 0xFF;

    static inline uchar user_blob_dilation_pix_size = 8;

    static inline uint  user_blob_max_allowed_x_distance = 8;
    static inline uint  user_blob_max_allowed_y_distance = 4;
    static inline uint  user_blob_min_allowed_pix_size = 100;

    //stat
    static inline bool   user_run_stat_command = false;

    //mask
    static inline string user_mask_conf_filename;

    //trajectory
    static inline bool   user_trajectory_on = false;

    static inline double user_trajectory_x_axis_ref;
    static inline double user_trajectory_y_axis_ref;

    static inline bool   user_trajectory_x_axis_x2_on = false;
    static inline bool   user_trajectory_x_axis_x_on = false;
    static inline double user_trajectory_x_axis_x2_coef;
    static inline double user_trajectory_x_axis_x_coef;
    static inline double user_trajectory_x_axis_c;

    static inline bool   user_trajectory_y_axis_x2_on = false;
    static inline bool   user_trajectory_y_axis_x_on = false;
    static inline double user_trajectory_y_axis_x2_coef;
    static inline double user_trajectory_y_axis_x_coef;
    static inline double user_trajectory_y_axis_c;

    //grab
    static inline bool   user_run_grab_command = false;
    static inline list<ulong> user_frame_position_list;
    static inline uint user_frame_position_percentage = 0;
    static inline uint user_frame_position_range = 1;
    //--------------------------------------------------------------------------
    //general
    static inline uint parallel_thread_count = 1;  //parallelization do not increase the performances due to: video file access and fast memory chages (frame updates)
    //--------------------------------------------------------------------------
    //masks
    static inline list<Rectangle> rectangle_mask_list;
    static inline list<Triangle>  triangle_mask_list;
    static inline list<Circle>    circle_mask_list;
    static inline list<Polygon>   polygon_mask_list;
    //--------------------------------------------------------------------------
    //---------------------------------------------------------------------------
};
//=============================================================================
#endif /* GLOBAL_VAR_H_ */
