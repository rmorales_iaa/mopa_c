/*
 * Frame.cpp
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */
//============================================================================
#include <bits/stdc++.h>
#include <immintrin.h>
//----------------------------------------------------------------------------
#include <opencv2/videoio.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
//----------------------------------------------------------------------------
#define MAX_VECTOR_SIZE 256
#include "vcl/vectorclass.h"
//----------------------------------------------------------------------------
#include "logger/minilog.h"
#include "global_var.h"
#include "util/util.h"
#include "frame/frame.h"
#include "mask/mask.h"
#include "queue/my_queue.h"
//============================================================================
using namespace std;
using namespace cv;
//============================================================================
//random number generator
static random_device my_random_device;
static mt19937_64 my_random_engine(my_random_device()); //Mersenne Twister 19937 (64 bits) engine
static uniform_int_distribution<int> frame_random_dist(10, 100);
//============================================================================
static const int reg_byte_size = sizeof(Vec32uc);
//============================================================================
bool Frame::raw_search_with_mask_avx2(ulong frame_index
                                    , uchar * frame_prev_data
                                    , uchar * frame_curr_data
                                    , uchar * frame_mask_data
                                    , ulong frame_byte_size
                                    , uchar user_search_frame_max_color_variation_allowed
                                    , uint x_axis_pix_count
                                    , uint mask_x_axis_min
                                    , uint mask_y_axis_min) { //slice_end is being processed

  MyQueue<ulong, FRAME_MAX_ALLOWED_IMPACT> queue; //frame_index, byte_pos, Comparison

  uint cmp;  //32 bits. Each bit refers to one char of Vec32uc. When using other vector size, update the size of this variable
  Vec32uc reg_curr(0); //32 x uchar
  Vec32uc reg_prev(0);
  Vec32uc reg_dif(0);
  Vec32uc reg_mask(0);
  ulong main_data_byte_pos = (frame_byte_size / reg_byte_size) *  reg_byte_size;
  ulong byte_pos = 0;

  //main data
  for(byte_pos = 0; byte_pos < main_data_byte_pos; byte_pos += reg_byte_size, frame_curr_data += reg_byte_size, frame_prev_data += reg_byte_size, frame_mask_data += reg_byte_size) {
    reg_curr.load(frame_curr_data);
	reg_prev.load(frame_prev_data);
	reg_mask.load(frame_mask_data);
	if (to_bits(reg_mask > 0)){
      reg_dif = select(reg_curr > reg_prev
	            , (reg_curr - reg_prev) > user_search_frame_max_color_variation_allowed
                    , (reg_prev - reg_curr) > user_search_frame_max_color_variation_allowed);
      cmp = to_bits(reg_dif > 0);
      if (cmp != 0){
        if (!queue.push(frame_index)) {
	      frame_discarded_count += 1;
		  return false; //no more impacts, bad frame
		}
		queue.push(byte_pos);
		queue.push(cmp);
	  }
	}
  }

  //remain data
  uchar dif;
  for(byte_pos = main_data_byte_pos; byte_pos < frame_byte_size; ++byte_pos, ++frame_curr_data, ++frame_prev_data, ++frame_mask_data){
     if (*frame_mask_data > 0){
       if( *frame_curr_data > *frame_prev_data ) dif = *frame_curr_data - *frame_prev_data;
       else dif = *frame_prev_data - *frame_curr_data;
       if (dif > user_search_frame_max_color_variation_allowed){
         if (!queue.push(frame_index)) {
	   frame_discarded_count += 1;
           return false; //no more impacts, bad frame
         }
         queue.push(byte_pos);
         queue.push(1);
       }
     }
  }

  //process the differences
  if (queue.size() > 0) {
    MINILOG(logINFO) << "Frame: " << frame_index << " has generated: " << queue.size() / 3 << " differences";
	while(!queue.is_empty()){
	  ulong frame_index = queue.pop();
	  ulong byte_pos = queue.pop();
	  uint cmp = queue.pop();
	  byte_pos += lsb_pos(cmp);
	  ulong pix_pos = byte_pos / 3;  //bytes per pixel
	  uint x = (pix_pos % x_axis_pix_count) + mask_x_axis_min;
	  uint y = pix_pos / x_axis_pix_count + mask_y_axis_min;
	  impact_manager.add(new Impact(frame_index, x, y));
	}
  }
  return true;
}
//============================================================================
bool Frame::raw_search_no_mask_avx2(ulong frame_index
                             , uchar * frame_prev_data
                             , uchar * frame_curr_data
                             , ulong frame_byte_size
                             , uchar user_search_frame_max_color_variation_allowed
							 , uint x_axis_pix_count
                             , uint x_axis_min_border
                             , uint x_axis_max_border
                             , uint y_axis_min_border
                             , uint y_axis_max_border) { //slice_end is being processed

  MyQueue<ulong, FRAME_MAX_ALLOWED_IMPACT> queue; //frame_index, byte_pos, Comparison

  uint cmp;  //32 bits. Each bit refers to one char of Vec32uc. When using other vector size, update the size of this variable
  Vec32uc reg_curr(0); //32 x uchar
  Vec32uc reg_prev(0);
  Vec32uc reg_dif(0);
  ulong main_data_byte_pos = (frame_byte_size / reg_byte_size) *  reg_byte_size;
  ulong byte_pos = 0;

  //main data
  for(byte_pos = 0; byte_pos < main_data_byte_pos; byte_pos += reg_byte_size, frame_curr_data += reg_byte_size, frame_prev_data += reg_byte_size) {
    reg_curr.load(frame_curr_data);
    reg_prev.load(frame_prev_data);
    reg_dif = select(reg_curr > reg_prev
                  , (reg_curr - reg_prev) > user_search_frame_max_color_variation_allowed
                  , (reg_prev - reg_curr) > user_search_frame_max_color_variation_allowed);
    cmp = to_bits(reg_dif > 0);

    if (cmp != 0){
      if (!queue.push(frame_index)) {
        frame_discarded_count += 1;
    	return false; //no more impacts, bad frame
      }
      queue.push(byte_pos);
      queue.push(cmp);
    }
  }

  //remain data
  uchar dif;
  for(byte_pos = main_data_byte_pos; byte_pos < frame_byte_size; ++byte_pos, ++frame_curr_data, ++frame_prev_data){
    if( *frame_curr_data > *frame_prev_data ) dif = *frame_curr_data - *frame_prev_data;
    else dif = *frame_prev_data - *frame_curr_data;
    if (dif > user_search_frame_max_color_variation_allowed){
      if (!queue.push(frame_index)) {
        frame_discarded_count += 1;
        return false; //no more impacts, bad frame
      }
      queue.push(byte_pos);
      queue.push(1);
    }
  }

  //process the differences
  if (queue.size() > 0) {
    MINILOG(logINFO) << "Frame: " << frame_index << " has generated: " << queue.size() / 3 << " differences";
    while(!queue.is_empty()){
      ulong frame_index = queue.pop();
      ulong byte_pos = queue.pop();
      uint cmp = queue.pop();
      byte_pos += lsb_pos(cmp);
      ulong pix_pos = byte_pos / 3; //bytes per pixel
      uint x = (pix_pos % x_axis_pix_count);
      uint y = pix_pos / x_axis_pix_count;

      //check if the impact is at the borders
      if((x <= x_axis_min_border) || (x >= x_axis_max_border)) continue;
      if((y <= y_axis_min_border) || (y >= y_axis_max_border)) continue;

      impact_manager.add(new Impact(frame_index, x, y));
    }
  }
  return true;
}
//============================================================================
void Frame::show(FrameDataType& frame, string window_name) {

  namedWindow(window_name, WINDOW_AUTOSIZE );// Create a window for display.
  imshow(window_name, frame);
  moveWindow(window_name,0, 0);
  waitKey(0);
  destroyAllWindows();
}
//============================================================================
void Frame::save(FrameDataType& frame, string filename) {

  //no color conversion required
  imwrite(filename, frame);
}
//=============================================================================
void Frame::save(My_video& video, FrameDataType& frame, ulong frame_index, string name, string time_stamp) {
  putText(frame
		  , time_stamp + " frame:" + std::to_string(frame_index)           //text
		  , Point(30,30)              //text_pos
		  , cv::FONT_HERSHEY_SIMPLEX  //text_font
		  , 0.50                      //text_scale
		  , Scalar(0,255,255));       // text_color;

  //save frame
  save(frame, name);
}

//============================================================================
string Frame::get_time_stamp_ms(My_video& video, ulong frame_index) {

  FrameDataType frame;
  video.get_frame(frame, frame_index);
  auto frame_relative_time_stamp_ms = (ulong) video.get_relative_time_stamp_ms();

  if (My_video::has_timestamp) {
    chrono::system_clock::time_point new_t = My_video::start_time_stamp + std::chrono::milliseconds(frame_relative_time_stamp_ms);
    return  serialize_time_point_ms(new_t, Impact::text_time_format);
  }
  else{
    uint sec = (uint) (frame_relative_time_stamp_ms / 1000);
    uint msec = (uint) (frame_relative_time_stamp_ms % 1000);
    return to_string(sec) + "s-" + to_string(msec) + "ms";
  }
}
//============================================================================
bool Frame::calculate_show_statistics(vector<uint> v){

  //sort the pixels
  sort(v.begin(), v.end());

  ulong acc = 0;
  for (uint& pix : v)
    acc += pix;

  float average = (float)acc / v.size();
  float variance = 0;
  for (uint& pix : v)
    variance += (average - pix) * (average - pix);
  variance /= v.size() - 1;  //is not a poblation
  float std_dev = sqrt(variance);

  stringstream average_string;
  average_string << fixed << std::setprecision(3) << average;

  stringstream std_dev_string;
  std_dev_string << fixed << std::setprecision(3) << std_dev;

  MINILOG(logINFO) << "Statistics ";
  MINILOG(logINFO) << "\ttotal pixels   : " << v.size();
  MINILOG(logINFO) << "\tmin pix value  : " << v.front();
  MINILOG(logINFO) << "\tmax pix value  : " << v.back();
  MINILOG(logINFO) << "\tpixels average : " << average_string.str();
  MINILOG(logINFO) << "\tpixels std_dev : " << std_dev_string.str();

  return true;
}
//============================================================================
bool Frame::calculate_show_statistics(FrameDataType& frame) {

  uchar * data = frame.data;
  uint k = 0;
  vector<uint> pix_seq;
  for(uint i=0; i < Frame::pix_count; ++i){
    int r = data[k];
    int g = data[k+1];
    int b = data[k+2];
    k+=3;
    uint luminance = (0.2126*r + 0.7152*g + 0.0722*b) + 0.5;
    pix_seq.push_back(luminance);
  }
  return calculate_show_statistics(pix_seq);
}
//============================================================================
void Frame::fill_rectangle_mask(FrameDataType& frame
		                     , uint x_min
		                     , uint x_max
		                     , uint y_min
		                     , uint y_max
		                     , uchar mask) {

  uchar * data = frame.data;
  uint pos=0;
  for(uint y = y_min; y <= y_max; ++y)
    for(uint x = x_min;  x <= x_max; ++x) {
      pos = (y * Frame::x_axis_pix_count * Frame::pix_byte_size) + (x * Frame::pix_byte_size);
      data[pos] = mask;
      data[pos+1] = mask;
      data[pos+2] = mask;
    }
}
//============================================================================
void Frame::fill_point_seq_mask(FrameDataType& frame
                               , list<Row_map>& row_list
                               , uchar mask) {
  uchar * data = frame.data;
  uint pos=0;
  uint y;
  uint max;

  for(auto& map: row_list)
    for(auto& [key, value]: map){
      y = key;
      max = value.get_segment_count();
      for(uint k=0; k < max; ++k){
        Segment s = value.get_segment(k);
        for(uint x = s.get_start(); x <= s.get_end(); ++x) {

       	  if (x > x_pix_max_index) continue;
          if (y > y_pix_max_index) continue;

          pos = get_pos(x, y);

          data[pos] = mask;
          data[pos+1] = mask;
          data[pos+2] = mask;
        }
      }
    }
}
//============================================================================
void Frame::get_drift(uint frame_index, float& x_drift, float& y_drift) {

  if (!Global_var::user_trajectory_on) return;
  if (Global_var::user_trajectory_x_axis_x2_on)
    x_drift = (Global_var::user_trajectory_x_axis_x2_coef * frame_index * frame_index) + (Global_var::user_trajectory_x_axis_x_coef * frame_index) + Global_var::user_trajectory_x_axis_c;
  else
    x_drift = (Global_var::user_trajectory_x_axis_x_coef * frame_index) + Global_var::user_trajectory_x_axis_c;

  if (Global_var::user_trajectory_y_axis_x2_on)
    y_drift = (Global_var::user_trajectory_y_axis_x2_coef * frame_index * frame_index) + (Global_var::user_trajectory_y_axis_x_coef * frame_index) + Global_var::user_trajectory_y_axis_c;
  else
    y_drift = (Global_var::user_trajectory_y_axis_x_coef * frame_index) + Global_var::user_trajectory_y_axis_c;

  x_drift = x_drift - Global_var::user_trajectory_x_axis_ref;
  y_drift = y_drift - Global_var::user_trajectory_y_axis_ref;
}
//============================================================================
FrameDataType Frame::get_sub_frame(FrameDataType& frame, uint x_min, uint y_min, uint x_max, uint y_max) {

  Rect r = Rect(x_min, y_min, x_max - x_min, y_max - y_min);
  return frame(r);
}
//============================================================================
void Frame::get_sub_frame_with_dritft(FrameDataType& frame, float x_drift, float y_drift, uint x_min, uint y_min, uint x_max, uint y_max) {

  int new_x_min = round(x_min + x_drift);
  int new_y_min = round(y_min + y_drift);

  int new_x_max = round(x_max + x_drift);
  int new_y_max = round(y_max + y_drift);

  if (new_x_min < 0) new_x_min = 0;
  else if (new_x_min > (int) (Frame::x_axis_pix_count -1)) new_x_min = Frame::x_axis_pix_count -1;

  if (new_y_min < 0) new_y_min = 0;
  else if (new_y_min > (int) (Frame::y_axis_pix_count -1)) new_y_min = Frame::y_axis_pix_count -1;


  if (new_x_max < 0) new_x_max = 0;
  else if (new_x_max > (int) (Frame::x_axis_pix_count -1)) new_x_max = Frame::x_axis_pix_count -1;

  if (new_y_max < 0) new_y_max = 0;
  else if (new_y_max > (int) (Frame::y_axis_pix_count -1)) new_y_max = Frame::y_axis_pix_count -1;

  frame = get_sub_frame(frame, new_x_min, new_y_min, new_x_max, new_y_max);
}
//-------------------------------------------------------------------------
void Frame::clip_point(uint& x, uint& y) {

  if (x > x_pix_max_index ) x = x_pix_max_index;
  if (y > y_pix_max_index ) y = y_pix_max_index;
}
//-------------------------------------------------------------------------
bool Frame::clip_rectangle(uint& x_min
                         , uint& y_min
                         , uint& x_max
                         , uint& y_max
						 , bool show_error) {

  if (x_min > x_max) {
    x_min = 0;
    x_max = 0;

    if (show_error) {MINILOG(logERROR) <<  "Error cropping rectangle: min(" << x_min << "," << y_min << ") max(" <<  x_max << "," << y_max << ")";}
    return false;
  }

  if (y_min > y_max) {
    y_min = 0;
    y_max = 0;

    if (show_error) { MINILOG(logERROR) <<  "Error cropping rectangle: min(" << x_min << "," << y_min << ") max(" <<  x_max << "," << y_max << ")";}
    return false;
  }

  if (x_max > x_pix_max_index ) x_max = x_pix_max_index;
  if (y_max > y_pix_max_index ) y_max = y_pix_max_index;
  return true;
}
//-------------------------------------------------------------------------
uint Frame::get_pos(uint x, uint y) {
  return (y * Frame::x_axis_pix_count * Frame::pix_byte_size) + (x * Frame::pix_byte_size);
}
//-------------------------------------------------------------------------
void Frame::get_pix_pos(uint slice_index
                      , uint slice_pos
                      , uint&x
		      , uint &y) {

  uint pix_pos = round((slice_index *  Frame::slice_byte_size) / (float) Frame::pix_byte_size);
  pix_pos += slice_pos;

  y = pix_pos / Frame::x_axis_pix_count;
  x = pix_pos % Frame::x_axis_pix_count;
}

//============================================================================
void Frame::update_search_parameters(void) {

  uint prev_slice_count = Frame::slice_count;
  uint frame_width =  Frame::mask_x_axis_max - Frame::mask_x_axis_min;
  uint frame_height = Frame::mask_y_axis_max - Frame::mask_y_axis_min;

  Frame::pix_count = frame_width * frame_height;
  Frame::byte_size = Frame::pix_count * Frame::pix_byte_size;
  Frame::slice_count = Frame::byte_size / Frame::slice_byte_size;
  Frame::apply_border_check = 0;

  float frame_count_percentage = 100 - ((Frame::slice_count / (float)prev_slice_count) * 100.0);
  stringstream frame_count_percentage_string;
  frame_count_percentage_string << fixed << std::setprecision(3) << frame_count_percentage;

  MINILOG(logINFO) << "\tMoving mask applied. New search parameters:";
  MINILOG(logINFO) << "\t\tFrame width             :" << frame_width << " pix";
  MINILOG(logINFO) << "\t\tFrame height            :" << frame_height << " pix";
  MINILOG(logINFO) << "\t\tFrame pix count         :" << Frame::pix_count << " pixels";
  MINILOG(logINFO) << "\t\tFrame byte size         :" << Frame::byte_size << " bytes";
  MINILOG(logINFO) << "\t\tFrame slice count(" << frame_count_percentage  << "% less) :" << Frame::slice_count << " slices";
  MINILOG(logINFO) << "\t\tNot applying border check";
}
//============================================================================
void Frame::init_frame_index_partition_list(void) {

  memset(frame_index_partition_list, 0, sizeof(frame_index_partition_list));

  uint range_size   = My_video::frame_total_count / Global_var::parallel_thread_count;
  uint remain_index = My_video::frame_total_count % Global_var::parallel_thread_count;
  uint k = 0;
  for(uint i = 0 ; i < Global_var::parallel_thread_count; ++i){
    frame_index_partition_list[k++] = (range_size * i);
    frame_index_partition_list[k++] = (range_size * (i+1));
  }
  frame_index_partition_list[k-1] += remain_index - 1;
}
//============================================================================
void Frame::init_frame_byte_partiton_list(void) {

  memset(frame_pix_partition_list, 0, sizeof(frame_pix_partition_list));

  uint partition_pix_size        = Frame::byte_size / Global_var::parallel_thread_count;
  uint remain_partition_pix_size = Frame::byte_size % Global_var::parallel_thread_count;
  uint k = 0;
  uint offset = 0;

  for(uint i = 0 ; i < Global_var::parallel_thread_count; ++i){
	frame_pix_partition_list[k++] = (partition_pix_size * i);
	frame_pix_partition_list[k++] = (partition_pix_size * (i+1)-1);
	++offset;
  }
  frame_pix_partition_list[k-1] -= - remain_partition_pix_size;
}
//============================================================================
bool Frame::save_user_frame(My_video& video, FrameDataType frame, FrameDataType frame_mask, ulong pos, string dir) {

  std::ostringstream pos_string;
  pos_string << std::setfill('0') << std::setw(6) << pos;

  string s =  dir + My_video::filename_no_path + "_frame_" + pos_string.str();
  if (Global_var::user_mask_on) {
   	Mask::apply_mask_regions(frame);
	if (Global_var::user_trajectory_on){
	  s+="_cropped_";
	  Mask::get_sub_frame_and_sub_mask(pos, frame, frame_mask, Global_var::user_trajectory_on);
    }
	else Frame::save(frame_mask, s+"_only_mask.png");
  }

  auto frame_relative_time_stamp_ms = (ulong) video.get_relative_time_stamp_ms();
  string time_stamp = "";

  if (My_video::has_timestamp) {
    chrono::system_clock::time_point new_t = My_video::start_time_stamp + std::chrono::milliseconds(frame_relative_time_stamp_ms);
    time_stamp = serialize_time_point_ms(new_t, "%Y-%m-%dT%H:%M:%S");

  }
  else{
    uint sec = (uint) (frame_relative_time_stamp_ms / 1000);
    uint msec = (uint) (frame_relative_time_stamp_ms % 1000);
    time_stamp = to_string(sec) + "s-" + to_string(msec) + "ms";
  }

  time_stamp = replace_all(time_stamp,":","-");

  s += "_" + time_stamp;
  s += ".png";
  Frame::save(video, frame, pos, s, time_stamp);
  MINILOG(logINFO) << "\tGrabbed frame: " << pos;
  return true;
}
//============================================================================
bool Frame::grab(My_video& video) {

  MINILOG(logINFO) << "Grabbing frames";
  MINILOG(logINFO) << "Checking the frames position to grab";

  //get the positions
  if (Global_var::user_frame_position_percentage > 0){  //fill the positions
    Global_var::user_frame_position_list.push_back(0); //add the first frame
    ulong chunk_size = round(My_video::frame_total_count / (100.0 /Global_var::user_frame_position_percentage));
    ulong pos = chunk_size;
    while(pos < (My_video::frame_max_index)){  //add the positions evenly spaced
      Global_var::user_frame_position_list.push_back(pos);
      pos += chunk_size;
    }
    Global_var::user_frame_position_list.push_back(My_video::frame_max_index); //add the last frame
  }

  //check positions
  MINILOG(logINFO) << "Processing : " << Global_var::user_frame_position_list.size() << " frames positions";
  for(auto& pos: Global_var::user_frame_position_list){
    if (pos > My_video::frame_max_index){
      MINILOG(logERROR) << "The position: " << pos << " is greater than the last frame position: " << My_video::frame_max_index;
      return false;
    }
  }

  FrameDataType frame;
  FrameDataType frame_mask;
  string dir = Global_var::user_output_directoy + "/" + "frame_grabbed" + "/";
  if (!exist_dir( dir )) make_path(dir);

  if (Global_var::user_mask_on ) {
    Mask::get_mask_limits();
   	Mask::get_mask(frame_mask);
   	Mask::apply_mask_regions(frame);
  }

  //process the positions
  if (Global_var::user_frame_position_range) {
	ulong min = Global_var::user_frame_position_list.front();
	ulong max = Global_var::user_frame_position_list.back();
    for(ulong pos = min; pos <= max; ++pos) {
	  if (!video.get_frame(frame, pos)) return false;
	    save_user_frame(video, frame, frame_mask, pos, dir);
    }
  }
  else {
    for(auto& pos: Global_var::user_frame_position_list){
      if (!video.get_frame(frame, pos)) return false;
      save_user_frame(video, frame, frame_mask, pos, dir);
    }
  }

  MINILOG(logINFO) << "Grabbed frames at directory : '" << dir << "'";

  return true;
}
//============================================================================
bool Frame::stat(My_video& video) {

  MINILOG(logINFO) << "Getting the statistics of the very first frame";

  FrameDataType frame;

  //load the very first frame
  video.get_frame(frame, 0);

  //check frame
  if(frame.empty()) {
    MINILOG(logERROR) << "Error getting the first frame";
    return false;
  }

  //calculate and print the stat
  return Frame::calculate_show_statistics(frame);
}
//============================================================================
//parallelize the frames (but not internally)
void Frame::search_parallel_frame_seq_raw(My_video& video, bool use_thread, uint thread_id) {

  uint frame_start_index = video.get_frame_start_index();
  uint frame_end_index = video.get_frame_end_index();

  if (use_thread) {MINILOG(logINFO) << "Starting thread: " << thread_id << " frame index range: [" << frame_start_index << "," << frame_end_index <<"]";}
  uint last_percentage_shown = -1;
  uint frame_total_count = frame_end_index - frame_start_index + 1;
  bool is_first_frame = true;
  FrameDataType frame_prev;

  for (uint i = frame_start_index; i <= frame_end_index; ++i) {

    FrameDataType frame_curr;
    FrameDataType frame_mask;

    //load the frame
    if (!video.get_frame(frame_curr)) {
      MINILOG(logERROR) << "Error getting the frame: " << i;
      break;
    }

    //apply mask
    if (Global_var::user_mask_on){
      Mask::get_mask(frame_mask);
      Mask::get_sub_frame_and_sub_mask(i, frame_curr, frame_mask, Global_var::user_trajectory_on);
    }
    //search for impacts
    if (is_first_frame) is_first_frame = false;
    else {  //specializated raw_search for optimization
      if (Global_var::user_mask_on)
        raw_search_with_mask_avx2(i
                                  , frame_prev.data
                                  , frame_curr.data
                                  , frame_mask.data
                                  , byte_size
                                  , Global_var::user_search_frame_max_color_variation_allowed
                                  , x_axis_pix_count
                                  , mask_x_axis_min
                                  , mask_y_axis_min);
        else
          raw_search_no_mask_avx2(i
                                , frame_prev.data
                                , frame_curr.data
                                , Frame::byte_size
                                , Global_var::user_search_frame_max_color_variation_allowed
                                , x_axis_pix_count
                                , x_axis_min_border
                                , x_axis_max_border
                                , y_axis_min_border
                                , y_axis_max_border);
    }

    //save previous frame
    frame_prev = frame_curr;

    //show progress
    uint percentage = (uint)(((i - frame_start_index) / (float) frame_total_count) * 100);
    if (last_percentage_shown != percentage) {
      if (use_thread) MINILOG(logINFO) << "Thread: " << thread_id << " frames processed: "<< percentage << "%";
      else MINILOG(logINFO) << "Frames processed: "<< percentage << "%";
      last_percentage_shown = percentage;
    }
  }
  if (use_thread){
    MINILOG(logINFO) << "Thread: " << thread_id << " has end";
  }
}
//============================================================================
void Frame::search_parallel_frame_seq(My_video& video) {

  //create the partition of the frames overlapping the last and first frame
  init_frame_index_partition_list();

  //create a pool of videos over the same file an sharing common data (see My_video.hpp class variable)
  //each video will process just only one item of the partition
  bool b;
  uint k = 0;
  My_video video_pool[Global_var::parallel_thread_count];
  for (uint i = 0; i < Global_var::parallel_thread_count; ++i){
	uint start_index = frame_index_partition_list[k++];
	uint end_index = frame_index_partition_list[k++];
    video_pool[i] = My_video(b, start_index, end_index);
    if(!b) {
      MINILOG(logINFO)  << "Can not open a thread video: "  << i;
      return;
    }
  }

  //start the thread pool
  thread thread_pool[Global_var::parallel_thread_count];
  for (uint i = 0; i < Global_var::parallel_thread_count; ++i){

    thread_pool[i] = thread(search_parallel_frame_seq_raw, std::ref(video_pool[i]), true, i);

    if ( i!=  Global_var::parallel_thread_count -1){
      std::chrono::milliseconds ms_to_sleep( frame_random_dist(my_random_engine) );
      MINILOG(logINFO)  << "Sleeping: " << ms_to_sleep.count() << "ms before starting a new thread";
      this_thread::sleep_for(ms_to_sleep);  //random start to not request at the same time the resource (video)
    }
  }

  //wait until the end of all threads in thread pool
  for (uint i = 0; i < Global_var::parallel_thread_count; ++i)
    thread_pool[i].join();
}
//============================================================================
bool Frame::search_impact_parallel(My_video& video, bool pixel_seq) {

  if (Global_var::user_mask_on ){  //moving mask, so calculate the new limits and update the search parameters
    Mask::get_mask_limits();
    update_search_parameters();
  }

  if (Global_var::parallel_thread_count == 1) search_parallel_frame_seq_raw(video, false, 0);
  else search_parallel_frame_seq(video);

  impact_manager.process(video);

  //print results
  MINILOG(logINFO) << "End of processing of video: '" << My_video::filename << "'";
  MINILOG(logINFO) << "Total detected impacts    : " << impact_manager.get_processed_impacts();

  return true;
}
//============================================================================
//parallelize the frames (but not internally)
void Frame::search_debug(std::string prev_frame_filename, std::string curr_frame_filename) {

  MINILOG(logDEBUG) << "Debug start: " << prev_frame_filename << " , " << curr_frame_filename;
  FrameDataType frame_prev = imread(prev_frame_filename);
  FrameDataType frame_curr = imread(curr_frame_filename);
  uint frame_index = 16005;

  raw_search_no_mask_avx2(frame_index
 		        , frame_prev.data
                        , frame_curr.data
                        , Frame::byte_size
                        , Global_var::user_search_frame_max_color_variation_allowed
                        , Frame::x_axis_pix_count
                        , Frame::x_axis_min_border
                        , Frame::x_axis_max_border
                        , Frame::y_axis_min_border
                        , Frame::y_axis_max_border);

  MINILOG(logDEBUG) << "Debug ends";
}
//============================================================================
void Frame::erode(FrameDataType& frame, uint erosion_size, uint erosion_elem) {

  int erosion_type = MORPH_RECT;
  if( erosion_elem == 0 ){ erosion_type = MORPH_RECT; }
  else if( erosion_elem == 1 ){ erosion_type = MORPH_CROSS; }
  else if( erosion_elem == 2) { erosion_type = MORPH_ELLIPSE; }

  Mat element = getStructuringElement( erosion_type,
                                       Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                       Point( erosion_size, erosion_size ) );

  /// Apply the erosion operation
  cv::erode( frame, frame, element);
}

//============================================================================
void Frame::dilate(FrameDataType& frame, uint dilation_size, uint dilation_elem) {

  int erosion_type = MORPH_RECT;
  if( dilation_elem == 0 ){ erosion_type = MORPH_RECT; }
  else if( dilation_elem == 1 ){ erosion_type = MORPH_CROSS; }
  else if( dilation_elem == 2) { erosion_type = MORPH_ELLIPSE; }

  Mat element = getStructuringElement( erosion_type,
                                       Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                       Point( dilation_size, dilation_size ) );

  /// Apply the dilate operation
  cv::dilate( frame, frame, element);
}

//============================================================================
void Frame::get_centred_rectangle(FrameDataType& frame_in, uint x, uint y, uint x_size, uint y_size, FrameDataType& frame_out) {

  uint max_x = x + x_size;
  uint min_x = x > x_size? (x - x_size) : 0;

  uint max_y = y + y_size;
  uint min_y = y > y_size? (y - y_size) : 0;

  clip_point(max_x, max_y);

  Rect rec(min_x, min_y, max_x - min_x, max_y - min_y);
  frame_out = FrameDataType(frame_in, rec);
}
//----------------------------------------------------------------------------
void Frame::get_average(FrameDataType& frame, uint& average_r, uint& average_g, uint& average_b) {

  ulong sum_r = 0;
  ulong sum_g = 0;
  ulong sum_b = 0;
  uchar * data = frame.data;
  ulong k = 0;
  ulong pix_count = frame.total();

  for(uint i=0; i < pix_count; ++i, k+=3){
   sum_r += data[k];
   sum_b += data[k+1];
   sum_g += data[k+3];
  }

  average_r = uint(sum_r / (float)frame.total());
  average_g = uint(sum_g / (float)frame.total());
  average_b = uint(sum_b / (float)frame.total());
}
//----------------------------------------------------------------------------
