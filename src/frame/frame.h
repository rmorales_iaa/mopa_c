/*
 * Frame.h
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef FRAME_H_
#define FRAME_H_
//=============================================================================
#include <vector>
#include <string>
#include <atomic>
//-----------------------------------------------------------------------------
#include "global_var.h"
#include "geometry/point/my_point2d.h"
#include "impact/impact_manager.h"
#include "video/my_video.h"
//=============================================================================
class My_video;  //forward declaration
//=============================================================================
#define FRAME_MAX_ALLOWED_IMPACT 100
//=============================================================================
class Frame {

  private:
    //-------------------------------------------------------------------------
    //mutex for parallel search
    static inline std::mutex search_mutex;
    //-------------------------------------------------------------------------
    //impact manager
    static inline Impact_manager impact_manager;
    //-------------------------------------------------------------------------
    static inline std::atomic<long> frame_discarded_count {0};
    //-------------------------------------------------------------------------
    //Each thread will process just one partition. The partitions have an overlap of one frame
    //All frames of the partition are compared with the previous one (except the very first)
    static inline uint frame_index_partition_list[1024];
    //-------------------------------------------------------------------------
    //Each thread will process just one partition of the same frame
    static inline uint frame_pix_partition_list[1024];
    //-------------------------------------------------------------------------
    static void init_frame_index_partition_list(void);
    //-------------------------------------------------------------------------
    static void init_frame_byte_partiton_list(void);
    //-------------------------------------------------------------------------
    static bool raw_search_with_mask_avx2(ulong frame_index
                                   , uchar * frame_prev_data
                                   , uchar * frame_curr_data
                                   , uchar * frame_mask_data
                                   , ulong frame_byte_size
                                   , uchar user_search_frame_max_color_variation_allowed
                                   , uint x_axis_pix_count
                                   , uint mask_x_axis_min
                                   , uint mask_y_axis_min);
    //-------------------------------------------------------------------------
    static bool raw_search_no_mask_avx2(ulong frame_index
                                 , uchar * frame_prev_data
                                 , uchar * frame_curr_data
                                 , ulong frame_byte_size
                                 , uchar user_search_frame_max_color_variation_allowed
                                 , uint x_axis_pix_count
                                 , uint x_axis_min_border
                                 , uint x_axis_max_border
                                 , uint y_axis_min_border
                                 , uint y_axis_max_border);
    //-------------------------------------------------------------------------
    static inline void search_parallel_frame_seq_raw(My_video& video
                                                   , bool use_thread
                                                   , uint thread_id);
    //-------------------------------------------------------------------------
    static inline void search_parallel_frame_seq(My_video& video);
    //-------------------------------------------------------------------------
    static inline void search_parallel_frame_pixel_seq(void);
    //-------------------------------------------------------------------------
    static inline void update_search_parameters(void);
    //-------------------------------------------------------------------------
    static bool save_user_frame(My_video& video, FrameDataType frame, FrameDataType frame_mask, ulong pos, string dir);
    //-------------------------------------------------------------------------
  public:
    //-------------------------------------------------------------------------
	//class variables (common to all objects oh this class)
    static inline uint pix_count = 0;

    static inline uint x_axis_pix_count = 0;
    static inline uint y_axis_pix_count = 0;

    static inline uint x_pix_max_index = 0;
    static inline uint y_pix_max_index = 0;

    static inline uint  pix_byte_size = 0;
    static inline ulong byte_size = 0;

    static inline string file_ouput_format;

    static inline float x_axis_border_percentage = 0;
    static inline float y_axis_border_percentage = 0;

    static inline uint  max_allowed_impact = 0;

    static inline ulong slice_count     = 0;
    static inline uint  slice_byte_size = 0;

    static inline bool apply_border_check = true;

    static inline uint x_axis_min_border = 0;
    static inline uint x_axis_max_border = 0;

    static inline uint y_axis_min_border = 0;
    static inline uint y_axis_max_border = 0;

    static inline uint mask_x_axis_min = 0;
    static inline uint mask_x_axis_max = 0;

    static inline uint mask_y_axis_min = 0;
    static inline uint mask_y_axis_max = 0;
    //-------------------------------------------------------------------------
    Frame(){};
    //-------------------------------------------------------------------------
    virtual ~Frame(){};
    //-------------------------------------------------------------------------
    static void show(FrameDataType& frame, string window_name="no name");
    //-------------------------------------------------------------------------
    static void save(FrameDataType& frame, string filename);
    //-------------------------------------------------------------------------
    static void save(My_video& video, FrameDataType& frame, ulong frame_index, string name,string time_stamp);
    //-------------------------------------------------------------------------
    static string get_time_stamp_ms(My_video& video, ulong frame_index);
    //-------------------------------------------------------------------------
    static long get_frame_discarded_count(void) {return frame_discarded_count;}
    //-------------------------------------------------------------------------
    static bool calculate_show_statistics(vector<uint> v);
    //-------------------------------------------------------------------------
    static bool calculate_show_statistics(FrameDataType& frame_curr);
    //-------------------------------------------------------------------------
    static inline uint get_pos(uint x, uint y);
    //------------------------------------------------------------------------double-
    static void get_pix_pos(uint slice_index
                          , uint slice_pos
                          , uint&x, uint &y);
    //-------------------------------------------------------------------------
    static void fill_rectangle_mask(FrameDataType& frame
    		                    , uint x_min
    		                    , uint x_max
    		                    , uint y_min
    		                    , uint y_max
    		                    , uchar mask);
    //-------------------------------------------------------------------------
    static void fill_point_seq_mask(FrameDataType& frame
                                  , list<Row_map>& row_list
                                  , uchar mask);
    //-------------------------------------------------------------------------
    static void get_drift(uint frame_index
                        , float& x_drift
                        , float& y_drift);
    //-------------------------------------------------------------------------
    static FrameDataType get_sub_frame(FrameDataType& frame
                                     , uint x_min
                                     , uint y_min
                                     , uint x_max
                                     , uint y_max);
    //-------------------------------------------------------------------------
    static void get_sub_frame_with_dritft(FrameDataType& frame
                                        , float x_drift
                                        , float y_drift
                                        , uint x_min
                                        , uint y_min
                                        , uint x_max
                                        , uint y_max);
    //-------------------------------------------------------------------------
    static bool clip_rectangle(uint& x_min
                             , uint& y_min
                             , uint& x_max
                             , uint& y_max
                             , bool show_error = false);
    //-------------------------------------------------------------------------
    static void clip_point(uint& x
                         , uint& y);
    //-------------------------------------------------------------------------
    static inline void get_drift(uint& frame_index
                               , uint& x_drift
                               , uint& y_drift);
    //-------------------------------------------------------------------------
    static bool stat(My_video& video);
    //-------------------------------------------------------------------------
    static bool grab(My_video& video);
    //-------------------------------------------------------------------------
    static bool search_impact_parallel(My_video& video, bool intra_frame = true);
    //-------------------------------------------------------------------------
    static void search_debug(std::string prev_frame_filename
                           , std::string curr_frame_filename);
    //-------------------------------------------------------------------------
    static void erode(FrameDataType& frame
                    , uint erosion_size
                    , uint erosion_elem);
    //-------------------------------------------------------------------------
    static void dilate(FrameDataType& frame
        	     , uint dilation_size
    		     , uint dilation_elem);
    //-------------------------------------------------------------------------
    static void get_centred_rectangle(FrameDataType& frame_in
				    , uint x
				    , uint y
				    , uint x_size
				    , uint y_size
				    , FrameDataType& frame_out);
    //-------------------------------------------------------------------------
    static void get_average(FrameDataType& frame
                          , uint& average_r
                          , uint& average_g
                          , uint& average_b);
    //-------------------------------------------------------------------------
};
//=============================================================================
#endif /* FRAME_H_ */
