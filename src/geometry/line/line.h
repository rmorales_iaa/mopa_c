/*
 * line.h
 *
 *  Created on: Nov 24, 2019
 *      Author: rafa
 */
//============================================================================
#ifndef GEOMETRY_LINE_LINE_H_
#define GEOMETRY_LINE_LINE_H_
//============================================================================
#include <cmath>
//----------------------------------------------------------------------------
#include "geometry/point/my_point2d.h"
//============================================================================
class Line
{
  //--------------------------------------------------------------------------
  private:
    float slope;
    float intercept;
    bool is_horizontal;
    bool is_vertical;
  //--------------------------------------------------------------------------
  public:
    //------------------------------------------------------------------------
    Line(My_point2D p_1, My_point2D p_2) {

      uint x1 =  p_1.get_x();
      uint y1 =  p_1.get_y();
      uint x2 =  p_2.get_x();
      uint y2 =  p_2.get_y();

      if (x2 == x1) {
	is_horizontal = false; //vertical
	is_vertical   = true;
	slope         = 1;
	intercept     = x1;
      }
      else {
        if (y2 == y1) {
	  is_horizontal = true;  //horizontal
	  is_vertical   = false;
	  slope         = 0;
	  intercept     = y1;
	}
        else {
          is_horizontal = false;
          is_vertical   = false;
          slope = (y2 - y1) / (float)(x2 - x1);
          intercept = y1 - (slope * x1);
         }
      }
    }
    //------------------------------------------------------------------------
    virtual ~Line(){};
    //------------------------------------------------------------------------
    uint operator () (uint x) {

      if(is_horizontal) return x;
      else
	if(is_vertical) {
	   if (x == intercept) return x;
	   return 0;
	}
	else return round((x * slope) + intercept);
    }
    //------------------------------------------------------------------------
};
//============================================================================
#endif /* GEOMETRY_LINE_LINE_H_ */
