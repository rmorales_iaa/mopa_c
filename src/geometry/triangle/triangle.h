/*
 * triangle.h
 *
 *  Created on: Nov 24, 2019
 *      Author: rafa
 */
//============================================================================
#ifndef GEOMETRY_TRIANGLE_TRIANGLE_H_
#define GEOMETRY_TRIANGLE_TRIANGLE_H_
//============================================================================
#include <vector>
#include <cmath>
//============================================================================
#include "geometry/geometry.h"
//=============================================================================
extern uint x_pix_max_index;
extern uint y_pix_max_index;
//============================================================================
class Triangle : public Geometry
{
  //--------------------------------------------------------------------------
  private:
    uint x0;
    uint y0;

    uint x1;
    uint y1;

    uint x2;
    uint y2;
   //--------------------------------------------------------------------------
  public:
   //--------------------------------------------------------------------------
    Triangle(uint _mask_inside_points
           , uint _x0
           , uint _y0
	   , uint _x1
	   , uint _y1
	   , uint _x2
	   , uint _y2
	   , bool& r) : Geometry(_mask_inside_points) {

      r = false;

      x0 = _x0;
      y0 = _y0;

      x1 = _x1;
      y1 = _y1;

      x2 = _x2;
      y2 = _y2;

      //calculate the formed area
      if (area() <= 0) return;
      r = true;
    }
    //------------------------------------------------------------------------
    Triangle(Triangle& t, int x_drift, int y_drift) : Geometry(t.mask_inside_points){

      x0 = t.x0;
      y0 = t.y0;

      x1 = t.x1;
      y1 = t.y1;

      x2 = t.x2;
      y2 = t.y2;
    }
    //------------------------------------------------------------------------
    virtual ~Triangle(){};
    //------------------------------------------------------------------------
    float area(void) {         //find area of triangle formed by p0, p1 and p2
      return (x0 *(y1 - y2) + x1 *(y2-y0) + x2 *(y0 - y1))/2;
    }
    //------------------------------------------------------------------------
    bool is_in(My_point2D p) {
      return is_in(p.get_x(), p.get_y());
    }
    //------------------------------------------------------------------------
    //https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
    //------------------------------------------------------------------------
    bool is_in(uint x, uint y) {

      if (x > x_pix_max_index) return false;
      if (y > y_pix_max_index) return false;

      int dX = x-x2;
      int dY = y-y2;

      int dX21 = x2 - x1;
      int dY12 = y1 - y2;

      int D = dY12 * (x0-x2) + dX21 * (y0-y2);
      int s = dY12 * dX + dX21 * dY;
      int t = (y2-y0) * dX + (x0-x2) * dY;
      if (D<0) return s<=0 && t<=0 && s+t>=D;
      return s>=0 && t>=0 && s+t<=D;
    }
    //------------------------------------------------------------------------
    void get_inside_point_list(Row_map& m, uint x_min, uint y_min, uint x_max, uint y_max) {

      get_limits(x_min, y_min, x_max, y_max);
      Geometry::calculate_inside_point_list(m, x_min, y_min, x_max, y_max);
    }
    //----------------------------------------------------------------------
    void get_limits(uint& x_min, uint& y_min, uint& x_max, uint& y_max) {

      x_min = min(min(x0, x1), x2);
      y_min = min(min(y0, y1), y2);

      x_max = max(max(x0, x1), x2);
      y_max = max(max(y0, y1), y2);
    }
    //------------------------------------------------------------------------
};
//============================================================================
#endif /* GEOMETRY_TRIANGLE_TRIANGLE_H_ */
