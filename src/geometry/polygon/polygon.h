/*
 * polygon.h
 *
 *  Created on: Dec 3, 2019
 *      Author: rafa
 */
//============================================================================
#ifndef GEOMETRY_POLYGON_POLYGON_H_
#define GEOMETRY_POLYGON_POLYGON_H_
//============================================================================
#include "cmath"
//============================================================================
#include "geometry/geometry.h"
//============================================================================
extern uint x_pix_max_index;
extern uint y_pix_max_index;
//============================================================================
using namespace std;
//============================================================================
class Polygon  : public Geometry
{
  //-------------------------------------------------------------------------
  private:
    inline const static double edge_error = 1.192092896e-07f; //see 'is_in'
    uchar flag;
    //----------------------------------------------------------------------
    vector<My_point2D> storage;
  //-------------------------------------------------------------------------
  public:
  //-------------------------------------------------------------------------
    Polygon(uint _mask_inside_points=1, uchar _flag=0) : Geometry(_mask_inside_points){ flag = _flag; }
    //-----------------------------------------------------------------------
    virtual ~Polygon(){flag = 0;};
    //-----------------------------------------------------------------------
    void add(My_point2D p) { storage.push_back(p); }
    //-----------------------------------------------------------------------
    void set_flag(uchar _flag){ flag = _flag; }
    //-----------------------------------------------------------------------
    uchar get_flag(void){return flag;}
    //-----------------------------------------------------------------------
    //https://stackoverflow.com/questions/11716268/point-in-polygon-algorithm
    bool is_in(My_point2D p) {

      uint x = p.get_x();
      uint y = p.get_y();
      uint i, j;
      bool r = false;
      uint point_count = storage.size();

      for (i = 0, j = point_count - 1; i < point_count; j = i++)
      {
        My_point2D pi = storage[i];
        My_point2D pj = storage[j];

        if (fabs((float)pi.get_y() -  (float) pj.get_y()) <= edge_error &&
            fabs((float)pj.get_y() -  (float) y)   <= edge_error &&
	        ((float)pi.get_x() >= (float) x)   != (pj.get_x() >= x))
          return true;

        if ((pi.get_y() > y) != (pj.get_y() > y)) {
          double c = ((float)pj.get_x() - (float)pi.get_x()) * ((float)y - (float)pi.get_y()) / ((float)pj.get_y() - (float)pi.get_y()) + pi.get_x();
          if (fabs((float) x - c) <= edge_error) return true;
          if (x < c) r = !r;
        }
      }
      return r;
    }
    //------------------------------------------------------------------------
    bool is_in(uint x, uint y) { return is_in(My_point2D(x,y)); }
    //------------------------------------------------------------------------
    void get_inside_point_list(Row_map& m, uint x_min, uint y_min, uint x_max, uint y_max) {

      get_limits(x_min, y_min, x_max, y_max);
      Geometry::calculate_inside_point_list(m, x_min, y_min, x_max, y_max);
    }
    //----------------------------------------------------------------------
    void get_limits(uint& x_min, uint& y_min, uint& x_max, uint& y_max) {

      x_min = x_pix_max_index;
      x_max = 0;

      y_min = y_pix_max_index;
      y_max = 0;

      for(auto &p: storage){
        x_min = min(x_min, p.get_x());
        y_min = min(y_min, p.get_y());

        x_max = max(x_max, p.get_x());
        y_max = max(y_max, p.get_y());
      }
    }
  //-------------------------------------------------------------------------
};
//============================================================================
#endif /* GEOMETRY_POLYGON_POLYGON_H_ */
