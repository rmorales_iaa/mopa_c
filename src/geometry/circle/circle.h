/*
 * circle.h
 *
 *  Created on: Nov 25, 2019
 *      Author: rafa
 */
//============================================================================
#ifndef GEOMETRY_CIRCLE_CIRCLE_H_
#define GEOMETRY_CIRCLE_CIRCLE_H_
//============================================================================
#include <cmath>
//============================================================================
#include "geometry/geometry.h"
#include "frame/frame.h"
//=============================================================================
extern uint x_pix_max_index;
extern uint y_pix_max_index;
//============================================================================
class Circle : public Geometry
{
  //--------------------------------------------------------------------------
  private:
    int x_centre;
    int y_centre;
    float radious;
  //--------------------------------------------------------------------------
  public:
    //------------------------------------------------------------------------
    Circle(uint _mask_inside_points
         , uint _x
         , uint _y
         , float _radious) : Geometry(_mask_inside_points) {

      x_centre = _x;
      y_centre = _y;
      radious = _radious;
    }
    //------------------------------------------------------------------------
    //https://stackoverflow.com/questions/4103405/what-is-the-algorithm-for-finding-the-center-of-a-circle-from-three-points
    Circle(uint _mask_inside_points
          , int x0
	  , int y0

	  , int x1
	  , int y1

	  , int x2
	  , int y2
	  , bool& b) : Geometry(_mask_inside_points) {

      b = false;
      x_centre = 0;
      y_centre = 0;
      radious = 0;

      float yDelta_a = y1 - y0;
      float xDelta_a = x1 - x0;
      float yDelta_b = y2 - y1;
      float xDelta_b = x2 - x1;

      float a_slope = yDelta_a / xDelta_a;
      float b_slope = yDelta_b / xDelta_b;

      float x_AB_Mid = (x0 + x1)/2.0;
      float y_AB_Mid = (y0 + y1)/2.0;

      float x_BC_Mid = (x1 + x2)/2.0;
      float y_BC_Mid = (y1 + y2)/2.0;

      if(yDelta_a == 0) { //a_slope == 0
        x_centre = x_AB_Mid;
        if (xDelta_b == 0) y_centre = y_BC_Mid; //b_slope == INFINITY
    	else y_centre = y_BC_Mid + (x_BC_Mid - x_centre) / b_slope;
      }
      else
        if (yDelta_b == 0) { //b_slope == 0
    	  x_centre = x_BC_Mid;
    	  if (xDelta_a == 0) y_centre = y_AB_Mid; //a_slope == INFINITY
    	  else y_centre = y_AB_Mid + (x_AB_Mid-x_centre) / a_slope;
    	}
    	else
    	  if (xDelta_a == 0) {        //a_slope == INFINITY
    	    y_centre = y_AB_Mid;
    	    x_centre = b_slope * (y_BC_Mid-y_centre) + x_BC_Mid;
    	  }
    	  else
    	    if (xDelta_b == 0) {        //b_slope == INFINITY
    	      y_centre = y_BC_Mid;
    	      x_centre = a_slope * (y_AB_Mid-y_centre) + x_AB_Mid;
    	    }
    	    else{
    	      x_centre = round((a_slope * b_slope * (y_AB_Mid - y_BC_Mid) - a_slope * x_BC_Mid + b_slope * x_AB_Mid) / (b_slope - a_slope));
    	      y_centre = round(y_AB_Mid - (x_centre - x_AB_Mid) / a_slope);
    	    }
      radious = distance(x0, y0);
      b = true;
    }
    //------------------------------------------------------------------------
    Circle(Circle& c, int x_drift, int y_drift) : Geometry(c.mask_inside_points){

      x_centre = c.x_centre + x_drift;
      y_centre = c.y_centre + y_drift;
      radious =  c.radious;
    }
    //------------------------------------------------------------------------
    virtual ~Circle(){};
    //------------------------------------------------------------------------
    float distance(int x, int y) {
      float xd = x_centre - x;
      float yd = y_centre - y;
      return sqrt((xd * xd) + (yd  * yd));
    }
    //------------------------------------------------------------------------
    bool is_in(uint x, uint y) {
      if (x > x_pix_max_index) return false;
      if (y > y_pix_max_index) return false;
      return distance(x, y) <= radious;
    }
    //------------------------------------------------------------------------
    int get_x_centre(void){return x_centre;}
    //------------------------------------------------------------------------
    int get_y_centre(void){return y_centre;}
    //------------------------------------------------------------------------
    float get_radious(void){return radious;}
    //------------------------------------------------------------------------
    void get_inside_point_list(Row_map& m, uint x_min, uint y_min, uint x_max, uint y_max) {

      uint c_x_min = max((int)x_min,(int)round(x_centre - radious));
      if (c_x_min > x_max) c_x_min = x_max;

      uint c_y_min = max((int)y_min,(int)round(y_centre - radious));
      if (c_y_min > y_max) c_y_min = y_max;

      uint c_x_max = min(x_max, (uint)round(x_centre + radious));
      if (c_x_max < x_min) c_x_max = x_min;

      uint c_y_max = min(y_max, (uint)round(y_centre + radious));
      if (c_y_max < y_min) c_y_max = y_min;

      Geometry::calculate_inside_point_list(m, x_min, y_min, x_max, y_max);
    }
    //----------------------------------------------------------------------
    void get_limits(uint& x_min, uint& y_min, uint& x_max, uint& y_max) {

      x_min =  round(sqrt((x_centre - radious) * (x_centre - radious)));
      x_max =  round(x_centre + radious);

      y_min =  round(sqrt((y_centre - radious) * (y_centre - radious)));
      y_max =  round(y_centre + radious);
    }
    //------------------------------------------------------------------------
};

//============================================================================
#endif /* GEOMETRY_CIRCLE_CIRCLE_H_ */
