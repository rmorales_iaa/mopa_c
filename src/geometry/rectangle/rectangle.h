
/*
 * rectangle.h
 *
 *  Created on: Nov 23, 2019
 *      Author: rafa
 */
//============================================================================
#ifndef GEOMETRY_RECTANGLE_RECTANGLE_H_
#define GEOMETRY_RECTANGLE_RECTANGLE_H_
//============================================================================
#include "geometry/geometry.h"
//============================================================================
class Rectangle : public Geometry
{
  //--------------------------------------------------------------------------
  private:
    uint x_min;
    uint y_min;

    uint x_max;
    uint y_max;
  //--------------------------------------------------------------------------
  public:
    //------------------------------------------------------------------------
    Rectangle(uint _mask_inside_points
            , uint _x_min
	    , uint _y_min
            , uint _x_max
            , uint _y_max
            , bool& r) : Geometry(_mask_inside_points) {

      r = false;
      if (_x_min > _x_max) return;
      if (_y_min > _y_max) return;

      x_min = _x_min;
      x_max = _x_max;

      y_min = _y_min;
      y_max = _y_max;

      r = true;
    }

    //------------------------------------------------------------------------
    Rectangle(Rectangle& r, int x_drift, int y_drift) : Geometry(r.mask_inside_points){

      x_min = r.x_min + x_drift;
      x_max = r.x_max + x_drift;

      y_min = r.y_min + y_drift;
      y_max = r.y_max + y_drift;
    }
    //-----------------------------------------------------------------------
    virtual ~Rectangle(){};
    //------------------------------------------------------------------------
    bool is_in(uint x, uint y) {
      if (x > x_pix_max_index) return false;
      if (y > y_pix_max_index) return false;
      return (x >= x_min) && (x <= x_max) && (y >= y_min) && (y <= y_max);
    }
    //------------------------------------------------------------------------
    void get_inside_point_list(Row_map& m, uint _x_min, uint _y_min, uint _x_max, uint _y_max) {

      get_limits(_x_min, _y_min, _x_max, _y_max);
      Geometry::calculate_inside_point_list(m, _x_min, _y_min, _x_max, _y_max);
    }
    //----------------------------------------------------------------------
    void get_limits(uint& _x_min, uint& _y_min, uint& _x_max, uint& _y_max) {

      _x_min = x_min;
      _y_min = y_min;

      _x_max = x_max;
      _y_max = y_max;
    }
    //----------------------------------------------------------------------
};
//============================================================================
#endif /* GEOMETRY_RECTANGLE_RECTANGLE_H_ */
