/*
 * geometry.h
 *
 *  Created on: 26 Nov 2019
 *      Author: rafa
 */
//============================================================================
#ifndef GEOMETRY_GEOMETRY_H_
#define GEOMETRY_GEOMETRY_H_
//============================================================================
#include <vector>
#include <cmath>
#include <map>
//============================================================================
#include "geometry/point/my_point2d.h"
#include "row_by_segment/row_by_segment.h"
//============================================================================

class Geometry
{
protected:
   //-----------------------------------------------------------------------
   uint mask_inside_points;
  //------------------------------------------------------------------------
  virtual void get_inside_point_list(Row_map& m, uint x_min, uint y_min, uint x_max, uint y_max) = 0;
  //------------------------------------------------------------------------
  void get_outside_point_list(Row_map& m, uint x_min, uint y_min, uint x_max, uint y_max) {

    uint max_seg;
    uint s_x_min;
    uint s_x_max;

    bool y_max_mask_initialized = false;
    uint y_min_mask = 0;
    uint y_max_mask = 0;

    //first get the inside points
    get_inside_point_list(m, x_min, y_min, x_max, y_max);

    //now get inverted segments
    for(auto& [y, rbs]: m){
	  std::vector<Segment> new_row;

	 //grab the used rows indexes
	  if (y_max_mask_initialized){
	    y_max_mask_initialized = true;
	    y_min_mask = y;
	    y_max_mask = y;
	  }
	  else{
	      if (y > y_max_mask) y_max_mask = y;
	      else
	        if (y < y_min_mask) y_min_mask = y;
	  }

	  //parse the rows getting the invert segments
	  max_seg = rbs.get_segment_count();
	  for(uint k=0; k < max_seg; ++k){
	    Segment s = rbs.get_segment(k);
	    s_x_min = s.get_start();
	    s_x_max = s.get_end();

	    Segment left_s =  Segment(x_min,max(x_min, s_x_min-1));
	    Segment right_s = Segment(min(x_max, s_x_max + 1), x_max);

	    new_row.push_back(left_s);
	    new_row.push_back(right_s);
  	  }
	  rbs.clear();
	  rbs.add(new_row);
    }

    //add the inverted of the used rows
    for(uint y = y_min; y < y_min_mask; ++y)
      m.insert(Row_pair(y, Row_by_segment(y, x_min, x_max)));

    for(uint y = y_min_mask + 1; y <= y_max; ++y)
      m.insert(Row_pair(y, Row_by_segment(y, x_min, x_max)));
  }
  //------------------------------------------------------------------------
  void calculate_inside_point_list(Row_map& m, uint x_min, uint y_min, uint x_max, uint y_max) {

    uint start_x;
    uint end_x;
    bool first_point_in_row;

    for(uint y = y_min; y <= y_max; ++y) {
      start_x = 0;
      end_x = 0;
      first_point_in_row = true;
      for(uint x = x_min; x <= x_max; ++x) {
        if (is_in(x, y)) {
          if (first_point_in_row) {
            start_x = x;
            end_x = x;
            first_point_in_row = false;
          }
          else {
            if((end_x + 1) == x)  end_x = x; //contiguous segment
            else { //no contiguous segment
              if (m.count(y) == 0)  m.insert(Row_pair(y, Row_by_segment(y,start_x, end_x))); //it was not stored previously
              else m.at(y).add_segment(start_x, end_x);
              start_x = x;
              end_x = x;
            }
          }
        }
      }
      if (!first_point_in_row) {
        if (m.count(y) == 0) m.insert(Row_pair(y, Row_by_segment(y,start_x, end_x))); //it was not stored previously
        else m.at(y).add_segment(start_x, end_x);
      }
    }
  }
  //------------------------------------------------------------------------
public:
  ///------------------------------------------------------------------------
  Geometry(uint _mask_inside_points){mask_inside_points = _mask_inside_points;}
  //------------------------------------------------------------------------
  virtual ~Geometry(){};
  //------------------------------------------------------------------------
  virtual bool is_in(uint x, uint y) = 0;
  //------------------------------------------------------------------------
  void get_point_list(list<Row_map>& row_list
		            , uint x_min
                    , uint y_min
		            , uint x_max
		            , uint y_max) {
    Row_map m;
    if (mask_inside_points) get_inside_point_list(m, x_min, y_min, x_max, y_max);
    else get_outside_point_list(m, x_min, y_min, x_max, y_max);
    row_list.push_back(m);
  }
  //------------------------------------------------------------------------
  virtual void get_limits(uint& x_min, uint& y_min, uint& x_max, uint& y_max) = 0;
  //------------------------------------------------------------------------
};
//============================================================================
#endif /* GEOMETRY_GEOMETRY_H_ */
