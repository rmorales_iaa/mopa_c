/*
 * util.h
 *
 *  Created on: Nov 16, 2019
 *      Author: rafa
 */
//============================================================================
#ifndef UTIL_H_
#define UTIL_H_
//============================================================================
#include <iostream>
#include <string>
#include <iomanip>
#include <chrono>
#include <ctime>
#include <cmath>
#include <algorithm>
#include <vector>
#include <array>
#include <sys/stat.h>
#include <errno.h>
#include <sys/time.h>
#include <filesystem>
#include <unistd.h>
//=============================================================================
static inline bool exist_dir(const std::string& path)
{
  struct stat info;
  if (stat(path.c_str(), &info) != 0) return false;
  return S_ISDIR(info.st_mode);
}

//=============================================================================
static inline bool exist_file(const std::string& path)
{
  struct stat info;
  if (stat(path.c_str(), &info) != 0) return false;
  return S_ISREG(info.st_mode);
}

//=============================================================================
static inline void delete_dir(const std::string& path)
{
  rmdir(path.c_str());
}
//=============================================================================
//https://stackoverflow.com/questions/675039/how-can-i-create-directory-tree-in-c-linux
static bool make_path(const std::string &path) {

  #if defined(_WIN32)
	int ret = _mkdir(path.c_str());
  #else
	mode_t mode = 0755;
	int ret = mkdir(path.c_str(), mode);
  #endif
	if (ret == 0) return true;
	switch (errno) {
	  case ENOENT:
		// parent didn't exist, try to create it
	  {
		uint pos = path.find_last_of('/');
		if (pos == std::string::npos)
        #if defined(_WIN32)
	      pos = path.find_last_of('\\');
		  if (pos == std::string::npos)
        #endif
		return false;
		if (!make_path(path.substr(0, pos))) return false;
	}
		// now, try to create again
  #if defined(_WIN32)
	return 0 == _mkdir(path.c_str());
  #else
	return 0 == mkdir(path.c_str(), mode);
  #endif

  case EEXIST:
		// done!
		return exist_dir(path);
	default:return false;
  }
}
//=============================================================================
static inline std::string get_parent_path(const std::string& s) {
  return s.substr(0, s.find_last_of("/\\"));
}

//=============================================================================
static inline std::string get_filename(const std::string& s) {

   char sep = '/';

#ifdef _WIN32
   sep = '\\';
#endif

   size_t i = s.rfind(sep, s.length());
   if (i != std::string::npos) {
      return(s.substr(i+1, s.length() - i));
   }

   return("");
}
//=============================================================================
static inline std::string system_command(const char* cmd) {
  std::array<char, 2048> buffer;
  std::string result;
  std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(cmd, "r"), pclose);
  if (!pipe) {
      throw std::runtime_error("popen() failed!");
  }
  while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) {
      result += buffer.data();
  }
  return result;
}
//=============================================================================
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

//=============================================================================
// trim from end (in place)
static inline void rtrim(std::string &s) {
  s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
      return !std::isspace(ch);
  }).base(), s.end());
}
//=============================================================================
// trim from both ends (in place)
static inline std::string trim(std::string &s) {
  ltrim(s);
  rtrim(s);
  return s;
}
//=============================================================================
static inline bool ends_with(const std::string& str, const std::string& suffix)
{
    return str.size() >= suffix.size() && 0 == str.compare(str.size()-suffix.size(), suffix.size(), suffix);
}
//=============================================================================
static inline bool starts_with(const std::string& str, const std::string& prefix)
{
    return str.size() >= prefix.size() && 0 == str.compare(0, prefix.size(), prefix);
}
//=============================================================================
static inline std::vector<std::string> split(const std::string& str, const std::string& delim)
{
  std::vector<std::string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        std::string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}
//=============================================================================
static inline bool is_number(const std::string& s) {
  return !s.empty() && std::find_if(s.begin(),
    s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}
//============================================================================
static inline uint get_pix(uint r, uint g, uint b) {
  return (r << 16) | (g << 8) | b;
}

//============================================================================
static inline bool is_odd(uint v) {
  return (v % 2 != 0);
}
//============================================================================
static inline bool is_even(uint v) {
  return (v % 2 == 0);
}
//============================================================================
//https://www.ridgesolutions.ie/index.php/2019/08/30/format-stdtime_point-in-iso-8601-format-with-fractional-seconds-microseconds-in-c/
static inline std::string get_time_iso_8601(std::chrono::time_point<std::chrono::system_clock> t) {

  // convert to time_t which will represent the number of
  // seconds since the UNIX epoch, UTC 00:00:00 Thursday, 1st. January 1970
	auto epoch_seconds = std::chrono::system_clock::to_time_t(t);

  // Format this as date time to seconds resolution
  // e.g. 2016-08-30T08:18:51
  std::stringstream stream;
  stream << std::put_time(gmtime(&epoch_seconds), "%FT%T");

  // If we now convert back to a time_point we will get the time truncated
  // to whole seconds
  auto truncated = std::chrono::system_clock::from_time_t(epoch_seconds);

  // Now we subtract this seconds count from the original time to
  // get the number of extra microseconds..
  auto delta_us = std::chrono::duration_cast<std::chrono::milliseconds>(t - truncated).count();

  // And append this to the output stream as fractional seconds
  // e.g. 2016-08-30T08:18:51.867479
  stream << "," << std::fixed << std::setw(3) << std::setfill('0') << delta_us << "Z";

  return stream.str();
}
//============================================================================
//https://stackoverflow.com/questions/2896600/how-to-replace-all-occurrences-of-a-character-in-string
static inline std::string replace_all(std::string str, const std::string& from, const std::string& to) {
  size_t start_pos = 0;
  while((start_pos = str.find(from, start_pos)) != std::string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
  }
  return str;
}
//=============================================================================
//https://stackoverflow.com/questions/34857119/how-to-convert-stdchronotime-point-to-string/34858704
static inline std::string serialize_time_point(const std::chrono::system_clock::time_point& time, const std::string& format)
{
    std::time_t tt = std::chrono::system_clock::to_time_t(time);
    std::tm tm = *std::gmtime(&tt); //GMT (UTC)

    std::stringstream ss;
    ss << std::put_time( &tm, format.c_str() );
    return ss.str();
}
//=============================================================================
static inline std::string serialize_time_point_ms(const std::chrono::system_clock::time_point& time, const std::string& format) {

  std::time_t tt = std::chrono::system_clock::to_time_t(time);
  std::tm tm = *std::gmtime(&tt); //GMT (UTC)
  const auto tt_ms = std::chrono::duration_cast<std::chrono::milliseconds>(time.time_since_epoch()) % 1000;

  std::stringstream ss;
  ss << std::put_time(&tm, format.c_str() ) << '.' << std::setfill('0') << std::setw(3) << tt_ms.count();
  return ss.str();
}
//=============================================================================
static inline std::string serialize_time_poin_now_ms(void) {

  // get a precise timestamp as a string
  const auto now = std::chrono::system_clock::now();
  const auto nowAsTimeT = std::chrono::system_clock::to_time_t(now);
  const auto nowMs = std::chrono::duration_cast<std::chrono::milliseconds>(
      now.time_since_epoch()) % 1000;
  std::stringstream nowSs;
  nowSs
      << std::put_time(std::localtime(&nowAsTimeT), "%a %b %d %Y %T")
      << '.' << std::setfill('0') << std::setw(3) << nowMs.count();
  return nowSs.str();
}
//============================================================================
static inline std::string get_time_iso_8601_now(void) {

  return get_time_iso_8601(std::chrono::system_clock::now());
}
//============================================================================
static inline std::string get_iso_time_string_simple_char(void){

  std::string s = get_time_iso_8601_now();
  return replace_all(s,":","-");
}
//============================================================================
static inline std::vector<std::string> get_file_seq_from_dir(const std::string& path, const std::string& pattern){

  std::vector<std::string> storage;
  for(auto& p: std::filesystem::recursive_directory_iterator(path)){
    if(p.path().extension() == pattern)
      storage.push_back(p.path());
  }
  return storage;
}
//============================================================================
//https://codeforces.com/blog/entry/10330
static inline uint msb_pos(uint x) {
  union { double a; int b[2]; };
  a = x;
  return (b[1] >> 20) - 1023;
}
//============================================================================
static inline uint lsb_pos(uint x) {
   return  __builtin_ffs(x);
}
//============================================================================
static inline std::string cpu_info(void) {

  return system_command("lscpu");
}
//=============================================================================
#endif /* UTIL_H_ */
