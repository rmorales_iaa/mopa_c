/*
 * gpu.h
 *
 *  Created on: Dec 4, 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef GPU_GPU_H_
#define GPU_GPU_H_
//=============================================================================
#include "opencv2/opencv.hpp"
//-----------------------------------------------------------------------------
#include "type_definition.h"
#include "global_var.h"
//-----------------------------------------------------------------------------
//#define __GPU_OPTIMIZATION_CODE
//-----------------------------------------------------------------------------
#ifdef __GPU_OPTIMIZATION_CODE
#include "opencv2/core/cuda.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/cudaarithm.hpp"
//=============================================================================
using namespace std;
using namespace cv::cuda;
//=============================================================================
class GPU
{
  //---------------------------------------------------------------------------
  public:
  //---------------------------------------------------------------------------
    GPU(){}
    //-------------------------------------------------------------------------
    virtual ~GPU(){}
    //-------------------------------------------------------------------------
    static void my_kernel(void);
    //-------------------------------------------------------------------------
    static void subtract(FrameDataType prev, FrameDataType curr, FrameDataType& dif, uint& maxPix) {

      GpuMat prevGpu(prev);
      GpuMat currGpu(curr);
      GpuMat difGpu(cv::Size(prev.cols, prev.rows), CV_8UC3);
      cv::cuda::absdiff(currGpu, prevGpu, difGpu);
      difGpu.download(dif);
    }
  //---------------------------------------------------------------------------
  //---------------------------------------------------------------------------
};

#endif /* GPU_OPTIMIZATION_CODE */
//=============================================================================
#endif /* GPU_GPU_H_ */

