/*
 * MyQueue.h
 *
 *  Created on: 25 Dec 2019
 *      Author: rafa
 *      Adapted from: https://www.agner.org/optimize/optimizing_cpp.pdf
 */
//=============================================================================
#ifndef QUEUE_MY_QUEUE_H_
#define QUEUE_MY_QUEUE_H_
//=============================================================================

template <typename T, unsigned int max_size>
class MyQueue {
  //---------------------------------------------------------------------------
  protected:
    T * head, * tail; // Pointers to current head and tail
    unsigned int n;            // Number of objects in list
    T list[max_size];  // Circular buffer
    //-------------------------------------------------------------------------
  public:
  //---------------------------------------------------------------------------
    MyQueue() { // Constructor

	  head = tail = list; // Initialize
	  n = 0;
	}
    //-------------------------------------------------------------------------
	bool push(T const & x) { // Put object into list

	  if (n >= max_size)  return false;// Return false if list full
	  n++; // Increment count
	  *head = x; //Copy x to list
	  head++; // Increment head pointer
	  if (head >= list + max_size) head = list;// Wrap around

	  return true;
	}
	//-------------------------------------------------------------------------
	T pop() { // Get object from list

	  if (n <= 0) return T(0); // Put an error message here: list empty
	  n--; // Decrement count
 	  T * p = tail; // Pointer to object
	  tail++; // Increment tail pointer
	  if (tail >= list + max_size) tail = list;// Wrap around
	  return *p;
	}
	//-------------------------------------------------------------------------
	unsigned int size() const { return n; }
	//-------------------------------------------------------------------------
    bool is_empty() const { return n == 0; }
  //---------------------------------------------------------------------------
};

//=============================================================================
#endif /* QUEUE_MY_QUEUE_H_ */
