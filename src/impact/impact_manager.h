/*
 * impact_manager.h
 *
 *  Created on: 18 nov. 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef IMPACT_MANAGER_H_
#define IMPACT_MANAGER_H_
//=============================================================================
#include <concurrent_storage/concurrent_queue.hpp>
#include <vector>
#include <math.h>
#include <sys/types.h>
#include <iostream>
#include <fstream>
//-----------------------------------------------------------------------------
#include "video/my_video.h"
#include "concurrent_storage/concurrent_queue.hpp"
#include "impact.h"
//-----------------------------------------------------------------------------
class My_video; //forward declaration
//=============================================================================
class Impact_manager {
  //---------------------------------------------------------------------------
  private :
    //-------------------------------------------------------------------------
    vector<Impact *> final_list;
    //-------------------------------------------------------------------------
    Concurrent_queue<Impact *> queue;
    //-------------------------------------------------------------------------
    static inline string blob_dir = "output/blob/";
    //-------------------------------------------------------------------------
    void stack(Impact * imp);
    //-------------------------------------------------------------------------
    string save(My_video& video
              , FrameDataType& frame
	          , ulong pos
	          , string suffix="");
    //-------------------------------------------------------------------------
    void generate_csv(My_video& video
		    , std::map<ulong, FrameDataType>& frame_map
		    , std::map<ulong, FrameDataType>& frame_prev_map);
    //-------------------------------------------------------------------------
    bool filter_frame_by_blob(My_video& video
		            , Impact * imp
		            , bool save_frames = false);
    //-------------------------------------------------------------------------
    void pre_processing(My_video& video);
    //-------------------------------------------------------------------------
    void draw_circles(My_video& video
		    , std::map<ulong, FrameDataType>& frame_map
		    , std::map<ulong, FrameDataType>& frame_prev_map);
    //-------------------------------------------------------------------------
    void find_moon(My_video& video
                , Impact * imp
                , cv::Point2d& moon_centre
                , float& moon_radius);
    //-------------------------------------------------------------------------
    ulong processed_impacts;
    //---------------------------------------------------------------------------
  public :
    Impact_manager(){processed_impacts = 0;};
    //-------------------------------------------------------------------------
    virtual ~Impact_manager();
    //-------------------------------------------------------------------------
    void add(Impact * imp){ queue.push(imp); }
    //-------------------------------------------------------------------------
    void process(My_video& video);
    //-------------------------------------------------------------------------
    inline ulong get_size(void) {return queue.size();}
    //-------------------------------------------------------------------------
    inline ulong get_processed_impacts(void) {return processed_impacts;}
    //-------------------------------------------------------------------------
};
//=============================================================================
#endif /* IMPACT_MANAGER_H_ */
