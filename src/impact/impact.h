/*
 * impact.h
 *
 *  Created on: Nov 18, 2019
 *      Author: rafa
 */
//============================================================================
#ifndef IMPACT_H_
#define IMPACT_H_
//============================================================================
#include <sys/types.h>
//----------------------------------------------------------------------------
#include <opencv2/core/types.hpp>
//----------------------------------------------------------------------------
#include "geometry/point/my_point2d.h"
//============================================================================
//============================================================================
class Impact
{
  //--------------------------------------------------------------------------
  public:
    //------------------------------------------------------------------------
	//class variables (common to all objects oh this class)
    static inline int    max_allowed_distance_pixel = 0;

    static inline Scalar circle_color = 0;
    static inline uint   circle_radius = 0;

    static inline Scalar text_color = 0;
    static inline cv::Point text_position = cv::Point(0,0); //Bottom-left corner of the text string in the image
    static inline int    text_font = 0;
    static inline float   text_scale = 0;
    static inline std::string text_time_format;
    //------------------------------------------------------------------------
    Impact(ulong _frame_index, uint _x, uint _y, bool _up_slope = false){

      p = My_point2D(_x, _y);
      frame_index=_frame_index;
      up_slope = _up_slope;
      finished = false;

      x_min = _x;
      x_max = _x;

      y_min = _y;
      y_max = _y;

    };
    //------------------------------------------------------------------------
    virtual ~Impact(){}
    //------------------------------------------------------------------------
    bool is_finishe(void) const {return finished;}
    //------------------------------------------------------------------------
    ulong get_frame_index(void) const {return frame_index;}
    //------------------------------------------------------------------------
    bool is_up_slope(void) const {return up_slope;}
    //------------------------------------------------------------------------
    My_point2D get_point(void) {return p;}
    //------------------------------------------------------------------------
    uint get_x(void) {return p.get_x();}
    //------------------------------------------------------------------------
    uint get_y(void) {return p.get_y();}
    //------------------------------------------------------------------------
    double get_distance(Impact* imp) {return p.distance(imp->p);}
    //------------------------------------------------------------------------
    void stack(Impact * imp) {
      x_min = std::min(x_min, imp->get_x());
      x_max = std::max(x_max, imp->get_x());

      y_min = std::min(y_min, imp->get_y());
      y_max = std::max(y_max, imp->get_y());

      p = My_point2D( x_min + round((x_max - x_min) / 2.0), y_min + round((y_max - y_min) / 2.0) );
      if (!imp->is_up_slope()) finished = true;
    }
    //------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  private:
    ulong frame_index;
    My_point2D p;

    uint x_min;
    uint x_max;

    uint y_min;
    uint y_max;

    bool up_slope;
    bool finished;
  //--------------------------------------------------------------------------
};
//============================================================================
#endif /* IMPACT_H_ */
//============================================================================
