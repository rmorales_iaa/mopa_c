/*
 * deletem.cpp
 *
 *  Created on: 18 nov. 2019
 *      Author: rafa
 */
//=============================================================================
#include <set>
//=============================================================================
#include <opencv2/imgproc.hpp>
//=============================================================================
#include "global_var.h"
#include "impact_manager.h"
#include "logger/minilog.h"
#include "impact.h"
#include "frame/frame.h"
#include "util/util.h"
#include "lunar_flash/lunar_flash.h"
#include "blob/blob.h"
//=============================================================================
using namespace std;
using namespace cv;
//=============================================================================
std::map<ulong, Blob> blob_map;
//=============================================================================
Impact_manager::~Impact_manager() {

  Impact * imp = NULL;
  while(!queue.empty()){
    queue.pop(imp);
    delete (imp);
  }
}
//=============================================================================
string Impact_manager::save(My_video& video, FrameDataType& frame, ulong frame_index, string suffix) {

  //show info
  string impact_info = "frame"+ suffix +": " + to_string(frame_index);

  std::ostringstream pos_string;
  pos_string << std::setfill('0') << std::setw(6) << frame_index;

  string dir = Global_var::user_output_directoy + "/frames/";
  if (!exist_dir( dir )) make_path(dir);
  string s =  dir + My_video::filename_no_path + "_frame_" + pos_string.str();

  //set the title
  string time_stamp = Frame::get_time_stamp_ms(video, frame_index);
  putText(frame, impact_info + " time_stamp: " + time_stamp, Impact::text_position, Impact::text_font, Impact::text_scale, Impact::text_color);

  //save frame
  Frame::save(frame, s + suffix + Frame::file_ouput_format);

  return time_stamp;
}
//=============================================================================
void Impact_manager::stack(Impact * new_imp) {

  Impact * imp;
  for (std::vector<Impact *>::iterator it = final_list.begin(); it != final_list.end(); it++) {
    imp = *it;
    if(imp->get_distance(new_imp) <= Impact::max_allowed_distance_pixel) {  //can stack?
      imp->stack(new_imp);
      delete new_imp;
      return;
    }
  }
  final_list.push_back(new_imp);
}
//=============================================================================
void Impact_manager::find_moon(My_video& video
                             , Impact * imp
		             , cv::Point2d& moon_centre
		             , float& moon_radius){

  FrameDataType frame;
  video.get_frame(frame, imp->get_frame_index());

  MINILOG(logINFO) << "\tSearching for moon centre ";
  Lunar_flash::search_moon_center(frame, imp->get_frame_index(), video.filename_no_path, moon_centre, moon_radius);

  if (moon_radius > 0) MINILOG(logINFO) << "\tMoon centre found at: (" << moon_centre.x << "," << moon_centre.x <<") radius: " << moon_radius ;
  else MINILOG(logINFO) << "\tCan not find moon centre";
}
//=============================================================================
void Impact_manager::generate_csv(My_video& video, std::map<ulong, FrameDataType>& frame_map, std::map<ulong, FrameDataType>& frame_prev_map){

  //open csv
   ofstream csv_file;
   string dir= Global_var::user_output_directoy + "/csv/";
   make_path(dir);
   string csv_filename = dir + My_video::filename_no_path + ".csv";
   Lunar_flash::open_csv(csv_file, csv_filename);

   //process the stacked impacts
   std::map<ulong, cv::Point2d> circle_centre_map;
   std::map<ulong, float> circle_radius_map;
   for (auto &imp : final_list) {
	  string time_stamp="";
      cv::Point2d moon_centre(0,0);
      float moon_radius = 0;
      ulong frame_index = imp->get_frame_index();
      MINILOG(logINFO) << "Processing impact : (" << imp->get_x() << "," << imp->get_y() << ") at frame: " << imp->get_frame_index();
      if (frame_map.count(frame_index) != 0) {

        //save frames
    	time_stamp = save(video, frame_map.at(frame_index), frame_index);
        save(video, frame_prev_map.at(frame_index-1), frame_index-1, "_PREV");

        //find moon
        //find_moon(video, imp, moon_centre, moon_radius);

        //remove frames from maps
        frame_map.erase(frame_index);
        frame_prev_map.erase(frame_index-1);

        //add circle info
        circle_centre_map.insert(std::pair <ulong, cv::Point2d>(frame_index, moon_centre));
        circle_radius_map.insert(std::pair <ulong, float>(frame_index, moon_radius));
      }
      else{
    	time_stamp = Frame::get_time_stamp_ms(video, frame_index);
        moon_centre = circle_centre_map.at(frame_index);
        moon_radius = circle_radius_map.at(frame_index);
      }

      Lunar_flash lf = Lunar_flash(video.filename_no_path
 	                         , time_stamp
                             , imp->get_frame_index()
 	                         , moon_centre.x
                             , moon_centre.y
 	                         , moon_radius
                             , imp->get_x()
 	                         , imp->get_y()
                             , imp->get_x() - moon_centre.x
                             , imp->get_y() - moon_centre.y);
     Lunar_flash::add_to_csv(csv_file,lf);
     delete(imp);
   }

   //close csv
   Lunar_flash::close_csv(csv_file, csv_filename);
}
//============================================================================
bool Impact_manager::filter_frame_by_blob(My_video& video, Impact * imp, bool save_frames){

  Blob blob;
  FrameDataType frame;
  ulong frame_index = imp->get_frame_index();

  if (blob_map.count(frame_index)) blob = blob_map.at(frame_index);
  else { //dilate the frame, find blobs, filter them and store inthe map
    video.get_frame(frame, frame_index);
    if (save_frames) Frame::save(frame, blob_dir + "frame_" + to_string(frame_index)+ ".png");
    Frame::dilate(frame, Global_var::user_blob_dilation_pix_size, MORPH_RECT);
    if (save_frames) Frame::save(frame, blob_dir + "frame_dilated_" + to_string(frame_index)+ ".png");
    blob = Blob(frame,Global_var::user_blob_max_allowed_x_distance, Global_var::user_blob_max_allowed_y_distance);
    blob.filter_blob(Global_var::user_blob_min_allowed_pix_size);
    blob_map.insert(std::pair <ulong, Blob>(frame_index, blob));

    if (save_frames) {
      blob.get_blob_mask(frame.rows, frame.cols, frame);
      Frame::save(frame, blob_dir + "frame_dilated_blob_mask_" + to_string(frame_index)+ ".png");
    }
  }

  bool blob_exist = blob.get_blob_count() > 0;
  if (blob_exist && blob.is_position_inside(imp->get_x(), imp->get_y())) return true;

  return false;
}
//============================================================================
void Impact_manager::pre_processing(My_video& video){

  MINILOG(logINFO) << "Processing : " <<  queue.size() << " individual differences. Filtering and stacking them when it was possible";
  Impact * imp = NULL;

  //blobs
  std::map<ulong, Blob> blob_map;
  bool save_blob_frames = Global_var::user_blob_save_frames;
  if(save_blob_frames){
    if(exist_dir(blob_dir)) delete_dir(blob_dir);
    make_path(blob_dir);
  }

  //parse differences
  while(!queue.empty()) {
    queue.pop(imp);

    if (Global_var::user_blob_filter_by_activated){
      if (filter_frame_by_blob(video, imp, save_blob_frames)) {
        MINILOG(logINFO) << "Ignored individual difference because it is in a brilliant zone: (" << imp->get_x() << "," << imp->get_y() << ") at frame: " << imp->get_frame_index();
        delete imp;
        continue;
      }
    }
    stack(imp);
  }

  blob_map.clear();
  MINILOG(logINFO) << "Final impact count : " <<  final_list.size();
}

//============================================================================
void Impact_manager::draw_circles(My_video& video, std::map<ulong, FrameDataType>& frame_map, std::map<ulong, FrameDataType>& frame_prev_map){

  ulong frame_index;
  for (auto &imp : final_list) {
    frame_index = imp->get_frame_index();
    FrameDataType frame;
    FrameDataType frame_prev;
    if (frame_map.count(frame_index) == 0) {  //frame not stored in the map
      video.get_frame(frame, frame_index);
      frame_map.insert(std::pair <ulong, FrameDataType>(frame_index, frame));

      video.get_frame(frame_prev, frame_index-1);
      frame_prev_map.insert(std::pair <ulong, FrameDataType>(frame_index-1, frame_prev));
    }
    else{ //frame previously stored
      frame = frame_map.at(frame_index);
      frame_prev = frame_prev_map.at(frame_index-1);
    }

    //add visual circle
    circle(frame, Point(imp->get_x(),imp->get_y()), Impact::circle_radius, Impact::circle_color);
    circle(frame_prev, Point(imp->get_x(),imp->get_y()), Impact::circle_radius, Impact::circle_color);
    ++processed_impacts;
  }
}
//============================================================================
void Impact_manager::process(My_video& video){

  if (queue.size() == 0){
    MINILOG(logWARNING) << "No impact detected";
    return;
  }

  pre_processing(video);

  //draw circles in frames (including multiple impacts)
  std::map<ulong, FrameDataType> frame_map;
  std::map<ulong, FrameDataType> frame_prev_map;

  draw_circles(video, frame_map, frame_prev_map);
  generate_csv(video, frame_map, frame_prev_map);

 }
//=============================================================================

