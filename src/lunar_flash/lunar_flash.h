/*
 * lunarflash.h
 *
 *  Created on: Dec 16, 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef VIDEO_LUNAR_FLASH_H_
#define VIDEO_LUNAR_FLASH_H_
//=============================================================================
#include <fstream>
#include <string>
//============================================================================
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
//============================================================================
#include "video/my_video.h"
#include "ransac/ransac_circle.h"
//============================================================================
using namespace std;
//=============================================================================
class Lunar_flash
{
  //---------------------------------------------------------------------------
  public:  
    //-------------------------------------------------------------------------
    Lunar_flash(string _video_name
              , string _time_stamp
              , uint _frame_index
              , int _x_pos_moon_centre
              , int _y_pos_moon_centre
              , float _moon_radius
              , int _x_pos_abs_image
              , int _y_pos_abs_image
              , int _x_pos_rel_image
              , int _y_pos_rel_image){

      video_name = _video_name;
      time_stamp = _time_stamp;
      frame_index =_frame_index;
      x_pos_moon_centre = _x_pos_moon_centre;
      y_pos_moon_centre = _y_pos_moon_centre;
      moon_radius = _moon_radius;
      x_pos_abs_image = _x_pos_abs_image;
      y_pos_abs_image = _y_pos_abs_image;
      x_pos_rel_image = _x_pos_rel_image;
      y_pos_rel_image = _y_pos_rel_image;
    }
    //-------------------------------------------------------------------------
    virtual ~Lunar_flash(){};
    //-------------------------------------------------------------------------
    static void cross_match(string dir_1, string dir_2, uint max_allowed_distance) {

      vector<string> file_seq_1 = get_file_seq_from_dir(dir_1, ".csv");
      vector<string> file_seq_2 = get_file_seq_from_dir(dir_2, ".csv");
      vector<Lunar_flash> storage_1;
      vector<Lunar_flash> storage_2;

      for(auto& f : file_seq_1){
        MINILOG(logINFO) << "Parsing file: " << f << "'";
        load_from_csv(f, storage_1);
      }

      for(auto& f : file_seq_2){
        MINILOG(logINFO) << "Parsing file: " << f << "'";
        load_from_csv(f, storage_2);
      }

      MINILOG(logINFO) << "Loaded : " << storage_1.size() << " flashes from: '" << dir_1 << "'";
      MINILOG(logINFO) << "Loaded : " << storage_2.size() << " flashes from: '" << dir_2 << "'";

      uint k = 1;
      for(auto& lf_1 : storage_1) {
        for(auto& lf_2 : storage_2) {
          My_point2D p_1(lf_1.x_pos_rel_image, lf_1.x_pos_rel_image);
          My_point2D p_2(lf_2.x_pos_rel_image, lf_2.x_pos_rel_image);
          if (p_1.distance(p_2) <= max_allowed_distance) {
            MINILOG(logINFO) << "Find common match: " << ++k;
            MINILOG(logINFO) << "\t'" << lf_1.video_name <<"' frame: " << lf_1.frame_index << "=>" <<  lf_1.video_name << "'" << lf_2.video_name <<"' frame: " << lf_2.frame_index;
          }
        }
      }

      if (k == 1) {MINILOG(logWARNING) << "No common match found";}
    }

    //-------------------------------------------------------------------------
    static void open_csv(ofstream& f, string filename){
      MINILOG(logINFO) << "Creating file: '" << filename <<"'";
      f.open(filename);
      f << "video,time_stamp,frame_index,x_pos_moon_centre,y_pos_moon_centre,moon_radius,x_pos_abs_image,y_pos_abs_image,x_pos_rel_image,y_pos_rel_image\n";
    }
    //-------------------------------------------------------------------------
    static void add_to_csv(ofstream& f, Lunar_flash& lf){
      f << lf.video_name << ",";
      f << lf.time_stamp << ",";
      f << lf.frame_index << ",";
      f << lf.x_pos_moon_centre << ",";
      f << lf.y_pos_moon_centre << ",";
      f << lf.moon_radius << ",";
      f << lf.x_pos_abs_image << ",";
      f << lf.y_pos_abs_image << ",";
      f << lf.x_pos_rel_image << ",";
      f << lf.y_pos_rel_image << "\n";
    }
    //-------------------------------------------------------------------------
    static void close_csv(ofstream& f, string filename){
      f.close();
      MINILOG(logINFO) << "Closed file: '" << filename <<"'";
    }
    //============================================================================
    //https://stackoverflow.com/questions/26222525/opencv-detect-partial-circle-with-noise
    //https://stackoverflow.com/questions/20698613/detect-semi-circle-in-opencv
    static bool search_moon_center(FrameDataType& frame_color
                                 , uint frame_index
                                 , string video_name
                                 , cv::Point2d& best_circle_centre
								 , float& best_circle_radius
                                 , bool show_limbo = false) {

      //generate the limbo
      std::string dir = Global_var::user_limbo_output_dir;
      if (!exist_dir(dir)) make_path(dir);
      string frame_name = dir + video_name + "_" + to_string(frame_index);
      string frame_input_name  = frame_name + ".png";
      string frame_output_name = frame_name + "_ony_limbo.png";
      Frame::save(frame_color, frame_input_name);
      string cmd = "java -jar input/jar/mopa-assembly-0.1.0-SNAPSHOT.jar " + frame_input_name + " " + frame_output_name;
      system_command(cmd.c_str());

      best_circle_centre.x = -1;
      best_circle_centre.y = -1;

      FrameDataType frame_limbo = imread(frame_output_name);
      FrameDataType frame_gray;
      cv::cvtColor(frame_limbo, frame_gray, cv::COLOR_BGR2GRAY);

      //get the non zero point list
      std::vector<cv::Point2d> point_list;

      //get the point not zero from image
      for(int y=0; y< frame_gray.rows; ++y)
        for(int x=0; x< frame_gray.cols; ++x)
          if(frame_gray.at<unsigned char>(y,x) > 0) point_list.push_back(cv::Point2d(x,y));

      Ransac_circle(Global_var::user_limbo_ransac_min_pix_radius
                  , Global_var::user_limbo_ransac_max_allowed_distance
                  , Global_var::user_limbo_ransac_max_loop_iteration
                  , Global_var::user_limbo_ransac_min_percentage_for_valid_circle
                  , best_circle_centre
                  , best_circle_radius
                  , point_list);

      if(best_circle_radius > 0) {
        cv::Mat tmp;
        frame_color.copyTo(tmp);
        cv::circle(tmp, best_circle_centre, best_circle_radius, cv::Scalar(255,255,0),1);
        Frame::save(tmp, frame_name + "_limbo.png");
        if (show_limbo) Frame::show(tmp);
      }
      else{
        best_circle_centre.x = 0;
    	best_circle_centre.y = 0;
    	best_circle_radius = -1;
      }

      return true;
    }
    //-------------------------------------------------------------------------
  private:
    //-------------------------------------------------------------------------
    static bool load_from_csv(string filename
                            , vector<Lunar_flash>& storage
			    , bool filter_radious_zero = true) {
      string s;
      ifstream f;
      f.open ( filename);
      if (!f.is_open()){
        MINILOG(logERROR) << "Can not open the file: '" << filename << "'";
        return false;
      }
      bool first_line = true;
      while(!f.eof()) {
        getline(f,s);
        if (first_line) first_line = false;  //avoid the first line
        else{
          if (s.empty())  continue;
          if (trim(s).empty())  continue;
          uint k = 0;
          auto seq = split(s,",");
          string video_name     =  trim(seq[k++]);
          string time_stamp     =  trim(seq[k++]);
          uint frame_index      =  stoi(trim(seq[k++]));
          int x_pos_moon_centre =  stoi(trim(seq[k++]));
          int y_pos_moon_centre =  stoi(trim(seq[k++]));
          float moon_radius     =  stof(trim(seq[k++]));
          if (filter_radious_zero && (moon_radius == 0)) continue;
          int x_pos_abs_image   =  stoi(trim(seq[k++]));
          int y_pos_abs_image   =  stoi(trim(seq[k++]));
          int x_pos_rel_image   =  stoi(trim(seq[k++]));
          int y_pos_rel_image   =  stoi(trim(seq[k++]));

          Lunar_flash lf(video_name
                       , time_stamp
                       , frame_index
                       , x_pos_moon_centre
                       , y_pos_moon_centre
                       , moon_radius
                       , x_pos_abs_image
                       , y_pos_abs_image
                       , x_pos_rel_image
                       , y_pos_rel_image);

          storage.push_back(lf);
        }
     }
     f.close();
     return true;
    }

    //-------------------------------------------------------------------------
    string video_name;
    string time_stamp;
    uint frame_index;
    int x_pos_moon_centre;
    int y_pos_moon_centre;
    float moon_radius;
    int x_pos_abs_image;
    int y_pos_abs_image;
    int x_pos_rel_image;
    int y_pos_rel_image;
    //-------------------------------------------------------------------------
};

//=============================================================================
#endif /* VIDEO_LUNAR_FLASH_H_ */
