/*
 * Mask.h
 *
 *  Created on: Dec 1, 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef MASK_MASK_H_
#define MASK_MASK_H_
//=============================================================================
#include <sys/types.h>
//=============================================================================
#include "util/util.h"
#include "frame/frame.h"
#include "global_var.h"
#include "logger/minilog.h"
//=============================================================================
class Mask
{
  //---------------------------------------------------------------------------
  public:
    //-------------------------------------------------------------------------
    Mask ();
    //-------------------------------------------------------------------------
    virtual ~Mask ();
    //-------------------------------------------------------------------------
    static void get_mask_limits(void) {

      parse_masked_regions();

      //create the test frame mask
      uint x_min = Frame::x_pix_max_index;
      uint x_max = 0;

      uint y_min = Frame::y_pix_max_index;
      uint y_max = 0;

      uint r_x_min = 0;
      uint r_y_min = 0;

      uint r_x_max = 0;
      uint r_y_max = 0;

      uint total_figures = 0;

      //rectangles
      for( auto& g : Global_var::rectangle_mask_list ){
    	g.get_limits(r_x_min, r_y_min, r_x_max, r_y_max);
        if (r_x_min < x_min) x_min = r_x_min;
        if (r_x_max > x_max) x_max = r_x_max;

        if (r_y_min < y_min) y_min = r_y_min;
        if (r_y_max > y_max) y_max = r_y_max;

        ++total_figures;
      }

      //triangles
      for( auto& g : Global_var::triangle_mask_list ){
    	g.get_limits(r_x_min, r_y_min, r_x_max, r_y_max);
        if (r_x_min < x_min) x_min = r_x_min;
        if (r_x_max > x_max) x_max = r_x_max;

        if (r_y_min < y_min) y_min = r_y_min;
        if (r_y_max > y_max) y_max = r_y_max;

        ++total_figures;
      }

      //circles
      for( auto& g : Global_var::circle_mask_list ){
        g.get_limits(r_x_min, r_y_min, r_x_max, r_y_max);
        if (r_x_min < x_min) x_min = r_x_min;
        if (r_x_max > x_max) x_max = r_x_max;

        if (r_y_min < y_min) y_min = r_y_min;
        if (r_y_max > y_max) y_max = r_y_max;

        ++total_figures;
      }

      //polygons
      for( auto& g : Global_var::polygon_mask_list ){
        g.get_limits(r_x_min, r_y_min, r_x_max, r_y_max);
        if (r_x_min < x_min) x_min = r_x_min;
        if (r_x_max > x_max) x_max = r_x_max;

        if (r_y_min < y_min) y_min = r_y_min;
        if (r_y_max > y_max) y_max = r_y_max;

        ++total_figures;
      }

      if (total_figures == 0){  //mask configuration file provided but no valid figures stored in
    	MINILOG(logWARNING) << "Mask configuration file provided : '" << Global_var::user_mask_conf_filename << "' but no valid figures stored in";
        Global_var::user_mask_on = false;
    	return;
      }

      Frame::clip_rectangle(x_min, y_min, x_max, y_max);

      assign_limits_and_show(x_min, y_min, x_max, y_max);
    }
    //-------------------------------------------------------------------------
    static void load_raw_bmp_mask(const char * s,  uchar * frame_mask){

      FILE * f = fopen(s, "rb");
      fseek(f, 54, SEEK_SET); //skip header
      fread(frame_mask, Frame::byte_size, 1, f);
      fclose(f);

      //the bmp pixels need to be vertically flipped to be aligned to frame
      flip_vertically(frame_mask, Frame::x_axis_pix_count, Frame::y_axis_pix_count, Frame::pix_byte_size);
     }

    //============================================================================
    static void get_mask(FrameDataType& frame) {
       apply_drift(frame, 0 , 0);
    }
    //============================================================================
    static void apply_drift(FrameDataType& frame, uint x_drift, uint y_drift) {

      parse_masked_regions();

      //set the frame with all ones
      frame = FrameDataType(cv::Size(Frame::x_axis_pix_count, Frame::y_axis_pix_count), CV_8UC3, Scalar::all(0xFFFFFF));

      //set the mask pixels with 0
      Frame::fill_point_seq_mask(frame, row_list, 0x00);
    }
    //============================================================================
    static void get_sub_frame_and_sub_mask(uint frame_index, FrameDataType& frame, FrameDataType& frame_mask, bool calculate_drfit = true){

      //get drift
      float x_drift = 0;
      float y_drift = 0;
      if (calculate_drfit) Frame::get_drift(frame_index, x_drift, y_drift);

      //apply drift
      Frame::get_sub_frame_with_dritft(frame
                                      , x_drift
                                      , y_drift
                                      , Frame::mask_x_axis_min
                                      , Frame::mask_y_axis_min
                                      , Frame::mask_x_axis_max
                                      , Frame::mask_y_axis_max);

      Frame::get_sub_frame_with_dritft(frame_mask
                                     , x_drift
                                     , y_drift
                                     , Frame::mask_x_axis_min
                                     , Frame::mask_y_axis_min
                                     , Frame::mask_x_axis_max
                                     , Frame::mask_y_axis_max);
    }

    //============================================================================
    static void apply_mask_regions(FrameDataType& frame) {

      MINILOG(logINFO) << "Applying mask on frame";
      parse_masked_regions();
      Frame::fill_point_seq_mask(frame, row_list, 0x00); //set the mask with 0
    }

  //---------------------------------------------------------------------------
  private:
    //-------------------------------------------------------------------------
    static inline list<Row_map> row_list;  //use inline modifier to initialize it
    //-------------------------------------------------------------------------
    static void flip_vertically(uchar *pixels, size_t width, size_t height, size_t bytes_per_pixel) {
      const size_t stride = width * bytes_per_pixel;
      uchar *row = new uchar[stride];
      uchar *low = pixels;
      uchar *high = &pixels[(height - 1) * stride];

      for (; low < high; low += stride, high -= stride) {
        memcpy(row, low, stride);
        memcpy(low, high, stride);
        memcpy(high, row, stride);
      }
      delete(row);
    }
    //-------------------------------------------------------------------------
    static void parse_masked_regions(void) {

      if (!row_list.empty()) return;

      //rectangles
      for( auto& rec : Global_var::rectangle_mask_list )
        rec.get_point_list(row_list, 0, 0, Frame::x_pix_max_index, Frame::y_pix_max_index);

      string suffix = "s";
      if (Global_var::rectangle_mask_list.size() > 0){
        if (Global_var::rectangle_mask_list.size() == 1)  suffix = "";
        MINILOG(logINFO) << "Parsed : " << Global_var::rectangle_mask_list.size() << " rectangle" + suffix;
      }

      //triangles
      suffix = "s";
      for( auto& t : Global_var::triangle_mask_list )
        t.get_point_list(row_list, 0, 0, Frame::x_pix_max_index, Frame::y_pix_max_index);

      if (Global_var::triangle_mask_list.size() > 0) {
        if (Global_var::triangle_mask_list.size() == 1)  suffix = "";
	    MINILOG(logINFO) << "Parsed : " << Global_var::triangle_mask_list.size() << " triangle" + suffix;
      }

      //circles
      suffix = "s";
      for( auto& c : Global_var::circle_mask_list ){
        c.get_point_list(row_list, 0, 0, Frame::x_pix_max_index, Frame::y_pix_max_index);
      }

      if (Global_var::circle_mask_list.size() > 0) {
	    if (Global_var::circle_mask_list.size() == 1)  suffix = "";
	    MINILOG(logINFO) << "Parsed : " << Global_var::circle_mask_list.size() << " circle" + suffix;
      }

      //polygons
      suffix = "s";
      for( auto& p : Global_var::polygon_mask_list ){
        p.get_point_list(row_list, 0, 0, Frame::x_pix_max_index, Frame::y_pix_max_index);
      }

      if (Global_var::polygon_mask_list.size() > 0) {
        if (Global_var::polygon_mask_list.size() == 1)  suffix = "";
        MINILOG(logINFO) << "Parsed : " << Global_var::polygon_mask_list.size() << " polygon" + suffix;
      }

    }
    //-------------------------------------------------------------------------
    static void assign_limits_and_show(uint x_min,  uint y_min, uint x_max, uint y_max) {

      Frame::mask_x_axis_min = x_min;
      Frame::mask_y_axis_min = y_min;

      Frame::mask_x_axis_max = x_max;
      Frame::mask_y_axis_max = y_max;

      MINILOG(logINFO) << "Mask min pix(" << Frame::mask_x_axis_min << "," << Frame::mask_y_axis_min <<")";
      MINILOG(logINFO) << "Mask max pix(" << Frame::mask_x_axis_max << "," << Frame::mask_y_axis_max <<")";

      uint width  = x_max - x_min;
      uint height = y_max - y_min;
      MINILOG(logINFO) << "Frame pix width  : " << width;
      MINILOG(logINFO) << "Frame pix height : " << height;
    }
    //-------------------------------------------------------------------------
  //---------------------------------------------------------------------------
};
//=============================================================================
#endif /* MASK_MASK_H_ */
