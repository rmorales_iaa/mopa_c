/*
 * type_definition.h
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */
//============================================================================
#ifndef TYPE_DEFINITION_H_
#define TYPE_DEFINITION_H_
  //=============================================================================
  #include <sys/types.h>
  //=============================================================================
  #include <opencv2/core.hpp>
  #include <opencv2/videoio.hpp>
  //--------------------------------------------------------------------------
  using namespace std;
  using namespace cv;
  //--------------------------------------------------------------------------
  typedef unsigned char uchar;
  typedef Mat FrameDataType;
  typedef Vec3b PixelDataType;
  typedef unsigned long long PixelComparisionDataType;
  //--------------------------------------------------------------------------
#endif /* TYPE_DEFINITION_H_ */
//============================================================================
