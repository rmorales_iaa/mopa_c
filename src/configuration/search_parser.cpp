/*
 * Configuration.cpp
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */
//=============================================================================
#include <configuration/search_parser.h>
#include <iostream>
#include <iomanip>
#include <cstdlib>
//=============================================================================
#include "logger/minilog.h"
#include "global_var.h"
//============================================================================
using namespace std;
using namespace libconfig;
//=============================================================================
Search_parser::Search_parser(string _filename, bool& r) {

  filename = _filename;
  r = false;

  // Read the file. If there is an error, report it and exit.
  try{ cfg.readFile(filename.c_str()); }
  catch(const FileIOException &fioex)
  {
    MINILOG(logERROR) << "Error opening configuration file:'" << _filename << "'";

	return;
  }
  catch(const ParseException &pex)
  {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    r = false;
    return;
  }

  Setting& root = cfg.getRoot();

  if(!parse_frame_section(root)) return;
  if(!parse_impact_section(root)) return;
  if(!parse_blob_section(root)) return;

  MINILOG(logINFO) << "Configuration file '" << filename <<"' parsed successfully";
  r = true;
}

//=============================================================================
string Search_parser::get_string(string key, bool& r) {

  try {
    string value = cfg.lookup( key );
    return value;
  }
  catch(const SettingNotFoundException &nfex)
  {
    MINILOG(logERROR) << "Error getting the key '" << key << "' is from configuration file '" << filename <<"'";
    r = false;
    return string("NOT SET");
  }
}
//=============================================================================
bool Search_parser::parse_frame_section(Setting& root) {

  int i;

  try {
    Setting &frame_section = root["Frame"];

    if (!frame_section.lookupValue("frame_max_allowed_impact", i)) {
      MINILOG(logERROR) << "Can not find setting: 'frame_max_allowed_impact' ";
      return false;
    }
    Frame::max_allowed_impact = i;

    if (!frame_section.lookupValue("frame_x_axis_border_percentage", i)) {
      MINILOG(logERROR) << "Can not find setting: 'frame_x_axis_border_percentage' ";
      return false;
    }
    Frame::x_axis_border_percentage = i;

    if (!frame_section.lookupValue("frame_y_axis_border_percentage", i)) {
      MINILOG(logERROR) << "Can not find setting: 'frame_y_axis_border_percentage' ";
      return false;
    }
    Frame::y_axis_border_percentage = i;

    if (!frame_section.lookupValue("frame_file_ouput_format", Frame::file_ouput_format)) {
      MINILOG(logERROR) << "Can not find setting: 'frame_file_ouput_format' ";
      return false;
    }

    return true;
  }
  catch(const SettingNotFoundException &nfex) {
    MINILOG(logERROR) << "Configuration setting not found in configuration file '" << filename
    		<< "'. Setting: '" << nfex.getPath() << "'";
	return false;
  }
  catch(const ParseException &pex) {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    return false;
  }
}
//=============================================================================
bool Search_parser::parse_impact_section(Setting& root) {

  vector<int> int_seq;
  string s;

  try {
    Setting &frame_section = root["Impact"];

    frame_section.lookupValue("impact_max_allowed_distance_pixel", Impact::max_allowed_distance_pixel);

	//impact_circle_color
    int_seq.clear();
    const Setting& color_settings = frame_section.lookup(string("impact_circle_color"));
    for (int k = 0; k < color_settings.getLength(); ++k)
	  int_seq.push_back(color_settings[k]);
    Impact::circle_color = Scalar(int_seq[0], int_seq[1], int_seq[2]);

    if (!frame_section.lookupValue("impact_circle_radius", Impact::circle_radius)) {
      MINILOG(logERROR) << "Can not find setting: 'impact_circle_radius' ";
      return false;
    }

    //impact_text_color
    int_seq.clear();
    const Setting& text_color = frame_section.lookup(string("impact_text_color"));
    for (int k = 0; k < text_color.getLength(); ++k)
      int_seq.push_back(text_color[k]);
    Impact::text_color = Scalar(int_seq[0], int_seq[1], int_seq[2]);

    //impact_text_position
    int_seq.clear();
    const Setting& text_position_settings = frame_section.lookup(string("impact_text_position"));
    for (int k = 0; k < text_position_settings.getLength(); ++k)
      int_seq.push_back(text_position_settings[k]);
    Impact::text_position = Point(int_seq[0], int_seq[1]);

	if (!frame_section.lookupValue("impact_text_font", Impact::text_font)) {
	  MINILOG(logERROR) << "Can not find setting: 'impact_text_font' ";
	  return false;
	}

	if (!frame_section.lookupValue("impact_text_scale", Impact::text_scale)) {
	  MINILOG(logERROR) << "Can not find setting: 'impact_text_scale' ";
	  return false;
	}
	if (!frame_section.lookupValue("impact_text_time_format", Impact::text_time_format)) {
	  MINILOG(logERROR) << "Can not find setting: 'impact_text_time_format' ";
	  return false;
	}

	return true;
  }
  catch(const SettingNotFoundException &nfex) {
    MINILOG(logERROR) << "Configuration setting not found in configuration file '" << filename
    		<< "'. Setting: '" << nfex.getPath() << "'";
	return false;
  }
  catch(const ParseException &pex) {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    return false;
  }
}
//=============================================================================
bool Search_parser::parse_blob_section(Setting& root) {

  int i;

  try {
    Setting &frame_section = root["Blob"];

    if (!frame_section.lookupValue("filter_by_blob_activated", i)) {
      MINILOG(logERROR) << "Can not find setting: 'filter_by_blob_activated' ";
      return false;
    }
    Global_var::user_blob_filter_by_activated = i;

    if (!frame_section.lookupValue("save_frames", i)) {
      MINILOG(logERROR) << "Can not find setting: 'save_frames' ";
      return false;
    }
    Global_var::user_blob_save_frames = i;

    if (!frame_section.lookupValue("blob_min_r", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_min_r' ";
      return false;
    }
    Global_var::user_blob_min_r = i;

    if (!frame_section.lookupValue("blob_min_g", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_min_g' ";
      return false;
    }
    Global_var::user_blob_min_g = i;

    if (!frame_section.lookupValue("blob_min_b", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_min_b' ";
      return false;
    }
    Global_var::user_blob_min_b = i;

    if (!frame_section.lookupValue("blob_max_r", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_min_r' ";
      return false;
    }
    Global_var::user_blob_max_r = i;

    if (!frame_section.lookupValue("blob_max_g", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_min_g' ";
      return false;
    }
    Global_var::user_blob_max_g = i;

    if (!frame_section.lookupValue("blob_max_b", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_min_b' ";
      return false;
    }
    Global_var::user_blob_max_b = i;

    if (!frame_section.lookupValue("blob_max_allowed_x_distance", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_max_allowed_x_distance' ";
      return false;
    }
    Global_var::user_blob_max_allowed_x_distance = i;

    if (!frame_section.lookupValue("blob_max_allowed_y_distance", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_max_allowed_y_distance' ";
      return false;
    }
    Global_var::user_blob_max_allowed_y_distance = i;

    if (!frame_section.lookupValue("blob_min_allowed_pix_size", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_min_allowed_pix_size' ";
      return false;
    }
    Global_var::user_blob_min_allowed_pix_size = i;

    if (!frame_section.lookupValue("blob_dilation_pix_size", i)) {
      MINILOG(logERROR) << "Can not find setting: 'blob_dilation_pix_size' ";
      return false;
    }
    Global_var::user_blob_dilation_pix_size = i;

    return true;
  }
  catch(const SettingNotFoundException &nfex) {
    MINILOG(logERROR) << "Configuration setting not found in configuration file '" << filename
    		<< "'. Setting: '" << nfex.getPath() << "'";
    return false;
  }
  catch(const ParseException &pex) {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    return false;
  }
}
//=============================================================================
