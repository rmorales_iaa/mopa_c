/*
 * Configuration.h
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */

//=============================================================================
#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_
//=============================================================================
#include <string>
#include <libconfig.h++>
//=============================================================================
using namespace std;
using namespace libconfig;
//=============================================================================
class Search_parser {
  private:
	string filename;
	Config cfg;
  public:
    //-------------------------------------------------------------------------
    Search_parser(string _filename, bool& r);
    //-------------------------------------------------------------------------
    virtual ~Search_parser(){};
    //-------------------------------------------------------------------------
    bool parse(void);
    //---------------------------------------------------------------------------
  private :
    bool parse_frame_section(Setting& root);
    bool parse_impact_section(Setting& root);
    bool parse_blob_section(Setting& root);
    //---------------------------------------------------------------------------
    string get_string(string s, bool& r);
    //---------------------------------------------------------------------------
};
//=============================================================================
#endif /* CONFIGURATION_H_ */
