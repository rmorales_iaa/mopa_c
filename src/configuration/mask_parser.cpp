/*
 * maskparser.cpp
 *
 *  Created on: Nov 23, 2019
 *      Author: rafa
 */

//=============================================================================
#include <iostream>
#include <iomanip>
#include <cstdlib>
#include <string>
//=============================================================================
#include "mask_parser.h"
#include "logger/minilog.h"
#include "global_var.h"
#include "geometry/rectangle/rectangle.h"
#include "geometry/triangle/triangle.h"
#include "geometry/circle/circle.h"
#include "geometry/polygon/polygon.h"
//============================================================================
using namespace std;
using namespace libconfig;
//=============================================================================
Mask_parser::Mask_parser(string _filename, bool& r) {

  filename = _filename;
  r = false;

  //Read the file. If there is an error, report it and exit.
  try { cfg.readFile(filename.c_str()); }
  catch(const FileIOException &fioex)
  {
    MINILOG(logERROR) << "Error opening configuration file:'" << _filename << "'";
    return;
  }
  catch(const ParseException &pex)
  {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    r = false;
    return;
  }

  Setting& root = cfg.getRoot();

  if (cfg.exists("Masks.trajectory")){
    if(!parse_trajectory_section(root)) return;
  }

  if (cfg.exists("Masks.rectangles")){
    if(!parse_rectangle_section(root)) return;
  }

  if (cfg.exists("Masks.triangles")){
    if(!parse_triangle_section(root)) return;
  }

  if (cfg.exists("Masks.circles")){
    if(!parse_circles_section(root)) return;
  }

  if (cfg.exists("Masks.polygons")){
    if(!parse_polygons_section(root)) return;
  }
  MINILOG(logINFO) << "Configuration file '" << filename <<"' parsed successfully";
  r = true;
}

//=============================================================================
bool Mask_parser::parse_trajectory_section(Setting& root) {

  uint x_axis_ref;
  uint y_axis_ref;

  float x_axis_x2 = 0;
  float x_axis_x = 0;
  float x_axis_c = 0;

  float y_axis_x2 = 0;
  float y_axis_x = 0;
  float y_axis_c = 0;

  try {
    Setting &trajectory_section = root["Masks"]["trajectory"];

    int count = trajectory_section.getLength();

    for(int i = 0; i < count; ++i) {

      const Setting &t = trajectory_section[i];

      if (!t.lookupValue("x_axis_ref", x_axis_ref)) {
        MINILOG(logERROR) << "Can not find setting: 'x_axis_ref' at line: " << t.getSourceLine() << " file: '" << t.getSourceFile() <<"'";
        return false;
      }

      if (!t.lookupValue("y_axis_ref", y_axis_ref)) {
        MINILOG(logERROR) << "Can not find setting: 'y_axis_ref' at line: " << t.getSourceLine() << " file: '" << t.getSourceFile() <<"'";
        return false;
      }

      if (t.lookupValue("x_axis_x2", x_axis_x2))
        Global_var::user_trajectory_x_axis_x2_on = true;

      if (!t.lookupValue("x_axis_x", x_axis_x)) {
        MINILOG(logERROR) << "Can not find setting: 'x_axis_x' at line: " << t.getSourceLine() << " file: '" << t.getSourceFile() <<"'";
        return false;
      } else Global_var::user_trajectory_x_axis_x_on = true;

      if (!t.lookupValue("x_axis_c", x_axis_c)) {
        MINILOG(logERROR) << "Can not find setting: 'x_axis_c' at line: " << t.getSourceLine() << " file: '" << t.getSourceFile() <<"'";
        return false;
      }

      if (t.lookupValue("y_axis_x2", y_axis_x2))
        Global_var::user_trajectory_y_axis_x2_on = true;

      if (!t.lookupValue("y_axis_x",y_axis_x)) {
        MINILOG(logERROR) << "Can not find setting: 'y_axis_x' at line: " << t.getSourceLine() << " file: '" << t.getSourceFile() <<"'";
        return false;
      } else Global_var::user_trajectory_y_axis_x_on = true;

      if (!t.lookupValue("y_axis_c", y_axis_c)) {
        MINILOG(logERROR) << "Can not find setting: 'y_axis_c' at line: " << t.getSourceLine() << " file: '" << t.getSourceFile() <<"'";
        return false;
      }

      Global_var::user_trajectory_x_axis_ref     = x_axis_ref;
      Global_var::user_trajectory_y_axis_ref     = y_axis_ref;

      Global_var::user_trajectory_x_axis_x2_coef = x_axis_x2;
      Global_var::user_trajectory_x_axis_x_coef  = x_axis_x;
      Global_var::user_trajectory_x_axis_c       = x_axis_c;

      Global_var::user_trajectory_y_axis_x2_coef = y_axis_x2;
      Global_var::user_trajectory_y_axis_x_coef  = y_axis_x;
      Global_var::user_trajectory_y_axis_c       = y_axis_c;
    }

    Global_var::user_trajectory_on = true;
    return true;
  }
  catch(const SettingNotFoundException &nfex) {
    MINILOG(logERROR) << "Configuration setting not found in configuration file '" << filename << "'. Setting: '" << nfex.getPath() << "'";
	return false;
  }
  catch(const ParseException &pex) {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    return false;
  }
}

//=============================================================================
bool Mask_parser::parse_rectangle_section(Setting& root) {

  uint x_min;
  uint x_max;

  uint y_min;
  uint y_max;

  uint mask_inside_points;
  bool r;

  try {
    Setting &rectangle_section = root["Masks"]["rectangles"];

    int count = rectangle_section.getLength();

    for(int i = 0; i < count; ++i) {
      const Setting &rectangle = rectangle_section[i];

      if (!rectangle.lookupValue("mask_inside_points", mask_inside_points)) {
        MINILOG(logERROR) << "Can not find setting: 'mask_inside_points' at line: " << rectangle.getSourceLine() << " file: '" << rectangle.getSourceFile() <<"'";
        return false;
      }

      if (!rectangle.lookupValue("x_min", x_min)) {
        MINILOG(logERROR) << "Can not find setting: 'x_min' at line: " << rectangle.getSourceLine() << " file: '" << rectangle.getSourceFile() <<"'";
        return false;
      }

      if (!rectangle.lookupValue("x_max", x_max)) {
        MINILOG(logERROR) << "Can not find setting: 'x_max' at line: " << rectangle.getSourceLine() << " file: '" << rectangle.getSourceFile() <<"'";
        return false;
      }

      if (!rectangle.lookupValue("y_min", y_min)) {
        MINILOG(logERROR) << "Can not find setting: 'y_min' at line: " << rectangle.getSourceLine() << " file: '" << rectangle.getSourceFile() <<"'";
        return false;
      }

      if (!rectangle.lookupValue("y_max", y_max)) {
        MINILOG(logERROR) << "Can not find setting: 'y_max' at line: " << rectangle.getSourceLine() << " file: '" << rectangle.getSourceFile() <<"'";
        return false;
      }
      Rectangle rec = Rectangle(mask_inside_points, x_min, y_min, x_max, y_max, r);
      if(!r){
	MINILOG(logERROR) << "Error in the definition of the rectangle. min value is greater then max value";
	return false;
      }

      Global_var::rectangle_mask_list.push_back( rec );
    }
    return true;
  }
  catch(const SettingNotFoundException &nfex) {
    MINILOG(logERROR) << "Configuration setting not found in configuration file '" << filename
    		<< "'. Setting: '" << nfex.getPath() << "'";
	return false;
  }
  catch(const ParseException &pex) {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    return false;
  }
}

//=============================================================================
bool Mask_parser::parse_triangle_section(Setting& root)  {

  uint x0;
  uint y0;

  uint x1;
  uint y1;

  uint x2;
  uint y2;

  uint mask_inside_points;
  bool r;

  try {
    Setting &triangle_section = root["Masks"]["triangles"];

    int count = triangle_section.getLength();

    for(int i = 0; i < count; ++i) {
      const Setting &triangle = triangle_section[i];

      if (!triangle.lookupValue("mask_inside_points", mask_inside_points)) {
        MINILOG(logERROR) << "Can not find setting: 'mask_inside_points' at line: " << triangle.getSourceLine() << " file: '" << triangle.getSourceFile() <<"'";
        return false;
      }

      if (!triangle.lookupValue("x0", x0)) {
        MINILOG(logERROR) << "Can not find setting: 'x0' at line: " << triangle.getSourceLine() << " file: '" << triangle.getSourceFile() <<"'";
      }

      if (!triangle.lookupValue("y0", y0)) {
        MINILOG(logERROR) << "Can not find setting: 'y0' at line: " << triangle.getSourceLine() << " file: '" << triangle.getSourceFile() <<"'";
        return false;
      }

      if (!triangle.lookupValue("x1", x1)) {
        MINILOG(logERROR) << "Can not find setting: 'x1' at line: " << triangle.getSourceLine() << " file: '" << triangle.getSourceFile() <<"'";
        return false;
      }

      if (!triangle.lookupValue("y1", y1)) {
        MINILOG(logERROR) << "Can not find setting: 'y1' at line: " << triangle.getSourceLine() << " file: '" << triangle.getSourceFile() <<"'";
        return false;
      }

      if (!triangle.lookupValue("x2", x2)) {
        MINILOG(logERROR) << "Can not find setting: 'x2' at line: " << triangle.getSourceLine() << " file: '" << triangle.getSourceFile() <<"'";
        return false;
      }

      if (!triangle.lookupValue("y2", y2)) {
        MINILOG(logERROR) << "Can not find setting: 'y2' at line: " << triangle.getSourceLine() << " file: '" << triangle.getSourceFile() <<"'";
        return false;
      }

      Triangle t = Triangle(mask_inside_points, x0, y0, x1, y1, x2, y2, r);
      if(!r){
        MINILOG(logERROR) << "Error in the definition of the triangle";
        return false;
      }
      Global_var::triangle_mask_list.push_back( t );
    }
    return true;
  }
  catch(const SettingNotFoundException &nfex) {
    MINILOG(logERROR) << "Configuration setting not found in configuration file '" << filename
    		<< "'. Setting: '" << nfex.getPath() << "'";
	return false;
  }
  catch(const ParseException &pex) {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    return false;
  }
}

//=============================================================================
bool Mask_parser::parse_circles_section(Setting& root)  {

  uint center_x;
  uint center_y;

  float radious;
  uint mask_inside_points;

  uint x0;
  uint y0;

  uint x1;
  uint y1;

  uint x2;
  uint y2;

  bool r;

  try {
    Setting &circle_section = root["Masks"]["circles"];

    int count = circle_section.getLength();

    for(int i = 0; i < count; ++i) {
      const Setting &circle = circle_section[i];

      if (!circle.lookupValue("mask_inside_points", mask_inside_points)) {
        MINILOG(logERROR) << "Can not find setting: 'mask_inside_points' at " << circle.getSourceLine() << " file '" << circle.getSourceFile() <<"'";
        return false;
      }

      if (circle.lookupValue("x0", x0)) { //definition by three points

        if (!circle.lookupValue("y0", y0)) {
    	  MINILOG(logERROR) << "Can not find setting: 'y0' at line: " << circle.getSourceLine() << " file: '" << circle.getSourceFile() <<"'";
    	  return false;
    	}

        if (!circle.lookupValue("x1", x1)) {
          MINILOG(logERROR) << "Can not find setting: 'x1' at line: " << circle.getSourceLine() << " file: '" << circle.getSourceFile() <<"'";
          return false;
        }

        if (!circle.lookupValue("y1", y1)) {
    	  MINILOG(logERROR) << "Can not find setting: 'y1' at line: " << circle.getSourceLine() << " file: '" << circle.getSourceFile() <<"'";
    	  return false;
    	}

        if (!circle.lookupValue("x2", x2)) {
          MINILOG(logERROR) << "Can not find setting: 'x2' at line:  " << circle.getSourceLine() << " file: '" << circle.getSourceFile() <<"'";
          return false;
        }

        if (!circle.lookupValue("y2", y2)) {
    	  MINILOG(logERROR) << "Can not find setting: 'y2' at line: " << circle.getSourceLine() << " file: '" << circle.getSourceFile() <<"'";
    	  return false;
    	}

        Circle c = Circle(mask_inside_points, x0, y0, x1, y1, x2, y2, r);
        if (!r){
          MINILOG(logERROR) << "The points used for circle definition are not a valif definition of circle: " << circle.getSourceLine() << " file: '" << circle.getSourceFile() <<"'";
          return false;
        }
        MINILOG(logINFO) << "Circle: p0(" << x0 <<"," << y0 <<"), " <<
                                     "p1(" << x1 <<"," << y1 <<"), " <<
                                     "p2(" << x2 <<"," << y2 <<") ";

        MINILOG(logINFO) << "\t centre: (" << to_string(c.get_x_centre()) << "," << to_string(c.get_y_centre()) << ")";
        stringstream radious_string;
        radious_string << fixed << std::setprecision(3) << c.get_radious();

        MINILOG(logINFO) << "\t radious: " << radious_string.str();
     	Global_var::circle_mask_list.push_back( c );
      }
      else { //definition by centre and radious

        if (!circle.lookupValue("center_x", center_x)) {
    	  MINILOG(logERROR) << "Can not find setting: 'center_x' at line: " << circle.getSourceLine() << " file: '" << circle.getSourceFile() <<"'";
    	  return false;
        }

	    if (!circle.lookupValue("center_y", center_y)) {
    	  MINILOG(logERROR) << "Can not find setting: 'center_y' at line: " << circle.getSourceLine() << " file: '" << circle.getSourceFile() <<"'";
    	  return false;
    	 }

    	if (!circle.lookupValue("radious", radious)) {
    	  MINILOG(logERROR) << "Can not find setting: 'radious' at line: " << circle.getSourceLine() << " file: '" << circle.getSourceFile() <<"'";
    	  return false;
    	}

    	Global_var::circle_mask_list.push_back( Circle(mask_inside_points, center_x, center_y, radious) );
      }
    }
    return true;
  }
  catch(const SettingNotFoundException &nfex) {
    MINILOG(logERROR) << "Configuration setting not found in configuration file '" << filename
    		<< "'. Setting: '" << nfex.getPath() << "'";
	return false;
  }
  catch(const ParseException &pex) {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    return false;
  }
}

//=============================================================================
bool Mask_parser::parse_polygons_section(Setting& root)  {

  uint mask_inside_points;

  uint x;
  uint y;

  try {
    Setting &polygons_section = root["Masks"]["polygons"];
    uint count = polygons_section.getLength();

    for(uint i = 0; i < count; ++i) {
     const Setting &polygon_section = root["Masks"]["polygons"][i];
     uint point_count = polygon_section.getLength();

     const Setting &p = polygon_section[0];
     if (!p.lookupValue("mask_inside_points", mask_inside_points)) {
       MINILOG(logERROR) << "Can not find setting: 'mask_inside_points' at " << p.getSourceLine() << " file '" << p.getSourceFile() <<"'";
       return false;
     }

     Polygon pol(mask_inside_points);

     for(uint k = 1; k < point_count; ++k) {
       const Setting &p = polygon_section[k];

       if (!p.lookupValue("x", x)) {
         MINILOG(logERROR) << "Can not find setting: 'x' at line: " << p.getSourceLine() << " file: '" << p.getSourceFile() <<"'";
         return false;
       }

       if (!p.lookupValue("y", y)) {
         MINILOG(logERROR) << "Can not find setting: 'y' at line: " << p.getSourceLine() << " file: '" << p.getSourceFile() <<"'";
         return false;
       }
       pol.add(My_point2D(x, y));
     }

     Global_var::polygon_mask_list.push_back(pol);
    }
    return true;
  }
  catch(const SettingNotFoundException &nfex) {
    MINILOG(logERROR) << "Configuration setting not found in configuration file '" << filename
    		<< "'. Setting: '" << nfex.getPath() << "'";
    return false;
  }
  catch(const ParseException &pex) {
    MINILOG(logERROR) << "Parse error at " << pex.getFile() << ":" << pex.getLine() << " - " << pex.getError();
    return false;
  }
}
//=============================================================================
