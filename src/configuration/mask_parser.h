/*
 * maskparser.h
 *
 *  Created on: Nov 23, 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef CONFIGURATION_MASK_PARSER_H_
#define CONFIGURATION_MASK_PARSER_H_
//=============================================================================
#include <string>
#include <libconfig.h++>
//=============================================================================
using namespace std;
using namespace libconfig;
//=============================================================================

class Mask_parser {
  private:
    string filename;
    Config cfg;
  public:
    //-------------------------------------------------------------------------
    Mask_parser(string _filename, bool& r);
    //-------------------------------------------------------------------------
    virtual ~Mask_parser(){};
    //-------------------------------------------------------------------------
    bool parse(void);
    //---------------------------------------------------------------------------
  private :
    bool parse_trajectory_section(Setting& root);
    bool parse_rectangle_section(Setting& root);
    bool parse_triangle_section(Setting& root);
    bool parse_circles_section(Setting& root);
    bool parse_polygons_section(Setting& root);
    //---------------------------------------------------------------------------
};
//=============================================================================
#endif /* CONFIGURATION_MASK_PARSER_H_ */

