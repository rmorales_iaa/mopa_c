//============================================================================
// Name        : mopa_C.cpp
// Author      : RMM
// Version     :
// Copyright   : Your copyright notice
// Description : MOon imPact Analyzer (mopa) c version
// All frames must have same size and must be aligned in order to be stacked
//============================================================================
#include <iostream>
#include <string>
#include <sys/types.h>
#include <sys/stat.h>
//----------------------------------------------------------------------------
#include "logger/minilog.h"
#include "version.h"
#include "util/util.h"
#include "global_var.h"
#include "command_line/command_line_parser.h"
#include "frame/frame.h"
#include "mask/mask.h"
#include "impact/impact_manager.h"
#include "lunar_flash/lunar_flash.h"
//============================================================================
using namespace std;
using namespace cv;
//============================================================================
//class specialization
//============================================================================
//local variables
//----------------------------------------------------------------------------
//frame mask
uchar * frame_mask = NULL;
//----------------------------------------------------------------------------
//max pixel indexes
uint x_pix_max_index = 0;
uint y_pix_max_index = 0;
bool has_avx2_instruction_set = false;
//============================================================================
void set_time_zone(void){

  //set time zone. See https://en.wikipedia.org/wiki/List_of_tz_database_time_zones
  setenv("TZ", "Etc/UTC", 1);
  tzset();
}
//============================================================================
void check_instruction_set(void){

  string info = cpu_info();
  MINILOG(logINFO) << "==========================================";
  MINILOG(logINFO) << "CPU capabilities :  \n" << trim(info);
  has_avx2_instruction_set  = info.find(" avx2 ")  !=std::string::npos;
  MINILOG(logINFO) << "==========================================";
  if (has_avx2_instruction_set) MINILOG(logINFO) << "Using avx2 registers";
  else MINILOG(logINFO) << "NOT Using avx2 registers";
  MINILOG(logINFO) << "==========================================";

}
//============================================================================
bool init_tasks(int argc, char *argv[]) {

  //set time zone
  set_time_zone();

  //init logger
  MiniLog::current_level() = logINFO;        // set debug level to INFO

  MINILOG(logINFO) << "Start of mopa_c version: " << MOPA_C_VERSION;

  check_instruction_set();
  if (!has_avx2_instruction_set){
    MINILOG(logINFO) << "The cpu of this computer has no avx2 instruction capabilities. Please use run mopa in a other computer";
    return false;
  }

  //parse user's input
  if (!Command_line_parser::parse(argc, argv)){
	MINILOG(logINFO) << "Error parsing the user's arguments";
    return false;
  }
  return true;
}
//============================================================================
void final_tasks(std::chrono::steady_clock::time_point start_time) {

  if (frame_mask != NULL) delete(frame_mask);

  auto processing_time = chrono::duration_cast<chrono::seconds> (chrono::steady_clock::now() - start_time);

  stringstream video_duration_string;
  video_duration_string << fixed << std::setprecision(3) << My_video::duration_s;

  stringstream ratio_string;
  ratio_string << fixed << std::setprecision(3) << processing_time.count() / My_video::duration_s;

  stringstream invalid_frame_count_string;
  invalid_frame_count_string << fixed << std::setprecision(3) <<  (Frame::get_frame_discarded_count() / (float)My_video::frame_total_count) * 100;

  MINILOG(logINFO) << "Discarded frame count  : " << Frame::get_frame_discarded_count() << " (ratio: " << invalid_frame_count_string.str() << "%)";
  MINILOG(logINFO) << "Ratio processing time(" << to_string(processing_time.count()) <<"s) / video time(" <<
      video_duration_string.str() << "s): " << ratio_string.str();
  MINILOG(logINFO) << "Elapsed time in seconds: " << processing_time.count();
}
//============================================================================
void show_context_info(bool show_opencv_info = false){

  MINILOG(logINFO) << "======================================================";
  MINILOG(logINFO) << "OpenCV version details: ";
  MINILOG(logINFO) << "\tOpenCV version      : " << CV_VERSION;
  MINILOG(logINFO) << "\tMajor version       : " << CV_MAJOR_VERSION;
  MINILOG(logINFO) << "\tMinor version       : " << CV_MINOR_VERSION;
  MINILOG(logINFO) << "\tSubminor version    : " << CV_SUBMINOR_VERSION;
  MINILOG(logINFO) << "\tOpenCV thread count : " << cv::getNumThreads();
  MINILOG(logINFO) << "\tUse optimized       : " << cv::useOptimized();
  MINILOG(logINFO) << "CPU features         : " << cv::getCPUFeaturesLine();
  MINILOG(logINFO) << "CPU core count       : " << cv::getNumberOfCPUs();
  if (show_opencv_info) {MINILOG(logINFO) << "\t" + cv::getBuildInformation();}
  MINILOG(logINFO) << "======================================================";
}
//============================================================================
void show_info(bool show_opencv_info = false){
  Command_line_parser::show_run_parameters();
  show_context_info(show_opencv_info);
}
//============================================================================
int main(int argc, char *argv[]) {

  auto start_time = chrono::steady_clock::now();

  //initial tasks and start
  if (init_tasks(argc, argv)) {

    show_info();

    bool b;
    My_video video(b);
    if(b){
      if (Global_var::user_run_search_command) Frame::search_impact_parallel(video);
      else
        if (Global_var::user_run_stat_command) Frame::stat(video);
        else
          if (Global_var::user_run_grab_command) Frame::grab(video);
          else MINILOG(logERROR) << "Unknown command";
      }

    //end tasks
    final_tasks(start_time);
  }
  MINILOG(logINFO) << "End of mopa_c version: " << MOPA_C_VERSION;

  return 0;
}
//============================================================================
