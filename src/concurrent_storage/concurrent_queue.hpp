//=============================================================================
//File:  Concurrent_queue.hpp
//=============================================================================
//! This class represents a concurrent queue.
// See: https://github.com/juanchopanza/cppblog/blob/master/Concurrency/Queue/Queue.h
/*! \author:  Rafael Morales 
 *  \mail:    rmorales@iaa.es
 *  \date:    10 April 2015
 *  \history: None
 */
//=============================================================================
//Conditional include of the header
#ifndef Concurrent_queue_H_
#define Concurrent_queue_H_

//-----------------------------------------------------------------------------
// System include
//-----------------------------------------------------------------------------
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>
//-----------------------------------------------------------------------------
// User include
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//  Class definitions
//-----------------------------------------------------------------------------

template<typename T>
class Concurrent_queue {
  //---------------------------------------------------------------------------
  public:
   //-------------------------------------------------------------------------
   T pop(void) {
     std::unique_lock<std::mutex> mlock(mutex);
     while (q.empty()){
       cond.wait(mlock);
     }
       auto val = q.front();
       q.pop();
       return val;
     }
   //-------------------------------------------------------------------------
   void pop(T &item) {
     std::unique_lock<std::mutex> mlock(mutex);
     while (q.empty()){
       cond.wait(mlock);
     }
     item = q.front();
     q.pop();
    }
   //-------------------------------------------------------------------------
   void front(T &item) {
     std::unique_lock<std::mutex> mlock(mutex);
     while (q.empty()){
       cond.wait(mlock);
     }
     item = q.front();
   }
   //-------------------------------------------------------------------------
   void push(const T &item) {
     std::unique_lock<std::mutex> mlock(mutex);
     q.push(item);
     mlock.unlock();
     cond.notify_one();
   }
   //-------------------------------------------------------------------------
   bool empty(void) { return q.empty(); }
   //-------------------------------------------------------------------------
   ulong size(void) { return q.size();}
   //-------------------------------------------------------------------------
   Concurrent_queue() = default;
   //-------------------------------------------------------------------------
   Concurrent_queue(const Concurrent_queue&) = delete;       // disable copying
   //-------------------------------------------------------------------------
   Concurrent_queue& operator=(const Concurrent_queue&) = delete; // disable assignment
   //-------------------------------------------------------------------------
private:
	std::queue<T> q;
	std::mutex mutex;
	std::condition_variable cond;
};

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

//End of conditional include
#endif /* Concurrent_queue_H_ */

//=============================================================================
//  End of file:  Concurrent_queue.hpp
//=============================================================================
