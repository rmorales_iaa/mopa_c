//=============================================================================
//File:  Concurrent_queue.hpp
//=============================================================================
//! This class represents a concurrent queue.
// See: https://github.com/juanchopanza/cppblog/blob/master/Concurrency/Queue/Queue.h
/*! \author:  Rafael Morales 
 *  \mail:    rmorales@iaa.es
 *  \date:    10 April 2015
 *  \history: None
 */
//=============================================================================
//Conditional include of the header
#ifndef Concurrent_vector_H_
#define Concurrent_vector_H_

//-----------------------------------------------------------------------------
// System include
//-----------------------------------------------------------------------------
#include <vector>
#include <thread>
#include <mutex>
#include <condition_variable>
//-----------------------------------------------------------------------------
// User include
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Namespace definitions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Type definitions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//  Class definitions
//-----------------------------------------------------------------------------

template<typename T>
class Concurrent_vector {
  //---------------------------------------------------------------------------
  public:
   //-------------------------------------------------------------------------
   T get(uint pos) {
     std::unique_lock<std::mutex> mlock(mutex);
     while (v.empty()){
       cond.wait(mlock);
     }
     return v[pos];
   }
   //-------------------------------------------------------------------------
   void set(uint pos, T item) {
     std::unique_lock<std::mutex> mlock(mutex);
     while (v.empty()){
       cond.wait(mlock);
     }
     v[pos] = item;
   }
   //-------------------------------------------------------------------------
   void add(const T &item) {
     std::unique_lock<std::mutex> mlock(mutex);
     v.push_back(item);
     mlock.unlock();
     cond.notify_one();
   }
   //-------------------------------------------------------------------------
   bool is_stored(uint pos) {
     bool b = false;
     std::unique_lock<std::mutex> mlock(mutex);
     if (pos > v.size() -1) b = false;
     else b = v[pos] != NULL;
     mlock.unlock();
     cond.notify_one();
     return b;
   }
   //-------------------------------------------------------------------------
   bool empty(void) { return v.empty(); }
   //-------------------------------------------------------------------------
   ulong size(void) { return v.size(); }
   //-------------------------------------------------------------------------
   Concurrent_vector() = default;
   //-------------------------------------------------------------------------
   Concurrent_vector(const Concurrent_vector&) = delete;       // disable copying
   //-------------------------------------------------------------------------
   Concurrent_vector& operator=(const Concurrent_vector&) = delete; // disable assignment
   //-------------------------------------------------------------------------
private:
   std::vector<T> v;
   std::mutex mutex;
   std::condition_variable cond;
};
//-------------------------------------------------------------------------
//End of conditional include
#endif /* Concurrent_vector_H_ */

//=============================================================================
//  End of file:  Concurrent_vector.hpp
//=============================================================================
