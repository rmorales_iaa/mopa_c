/*
 * mopa_c.h
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */

#ifndef MOPA_VERSION_H_
#define MOPA_VERSION_H_
//============================================================================
#define MOPA_C_VERSION_DATE         "Wed 28 Apr 12:28:52 CEST 2021"
#define MOPA_C_VERSION_MAYOR        "0"
#define MOPA_C_VERSION_MINOR        "1"
#define MOPA_C_VERSION_COMPILATION  "118"
#define MOPA_C_VERSION              MOPA_C_VERSION_MAYOR "." MOPA_C_VERSION_MINOR "." MOPA_C_VERSION_COMPILATION "(" MOPA_C_VERSION_DATE ")"
//============================================================================
#endif /* MOPA_VERSION_H_ */
