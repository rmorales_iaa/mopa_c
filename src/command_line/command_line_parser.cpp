/*
 * Commandlineparser.cpp
 *
 *  Created on: 19 nov. 2019
 *      Author: rafa
 */
//=============================================================================
//=============================================================================
#include <vector>
//=============================================================================
#include "global_var.h"
#include "util/util.h"
#include "logger/minilog.h"
#include "version.h"
#include "command_line_parser.h"
#include "configuration/search_parser.h"
#include "configuration/mask_parser.h"
//=============================================================================
using namespace std;
using namespace cxxopts;
//============================================================================
void show_examples(void){

  MINILOG(logINFO) << "----------------------------------------------";
  MINILOG(logINFO) << "Example 01 : Calculate statistics                                              : mopa_c -i /my_video.avi --stats ";
  MINILOG(logINFO) << "Example 02 : Search impact with no mask                                        : mopa_c -i /my_video.avi --search ";
  MINILOG(logINFO) << "Example 03 : Search impact with no mask using 4 threads                        : mopa_c -i /my_video.avi --search -t 4";
  MINILOG(logINFO) << "Example 04 : Search impact with no mask and color variation 60                 : mopa_c -i /my_video.avi --search -c 60";
  MINILOG(logINFO) << "Example 04 : Search impact with no mask using 4 threads and color variation 60 : mopa_c -i /my_video.avi --search -t 4 -c 60";
  MINILOG(logINFO) << "Example 05 : Search impact with fixed mask                                     : mopa_c -i /my_video.avi --search -m /my_mask.conf";
  MINILOG(logINFO) << "Example 06 : Search impact with mask and trajectory                            : mopa_c -i /my_video.avi --search -m /my_mask.conf";
  MINILOG(logINFO) << "Example 07 : Grab the frame 1                                                  : mopa_c -i /my_video.avi --grab -p 1";
  MINILOG(logINFO) << "Example 08 : Grab the frames 1 and 5                                           : mopa_c -i /my_video.avi --grab -p 1,5";
  MINILOG(logINFO) << "Example 09 : Grab the frames from 1 to 5 (both included)                       : mopa_c -i /my_video.avi --grab -p 1-5";
  MINILOG(logINFO) << "Example 10 : Grab 4 frames at 25% position                                     : mopa_c -i /my_video.avi --grab -p 25%";
  MINILOG(logINFO) << "Example 11 : Grab 4 frames at 25% position with trajectory                     : mopa_c -i /my_video.avi --grab -p 25% -k /my_mask.conf'";
  MINILOG(logINFO) << "Note: When the mask configuration file had a trajectory, it will be used automatically (examples 4 and 8)";
  MINILOG(logINFO) << "----------------------------------------------";

}
//=============================================================================
bool Command_line_parser::parse(int argc, char *argv[]){

  Options options("mopa", "MOon imPact Analyzer. Rafael Morales IAA-CSIC. 2019-2020. Version:" + string(MOPA_C_VERSION));

  //argument group definition
  const string GROUP_COMMON_OPTIONS         = "common options";
  const string GROUP_SEARCH_COMMAND_OPTIONS = "search command";
  const string GROUP_STAT_COMMAND_OPTIONS   = "stat command";
  const string GROUP_GRAB_COMMAND_OPTIONS   = "grab command";

  const vector<string> group_list = {GROUP_COMMON_OPTIONS
                                   , GROUP_SEARCH_COMMAND_OPTIONS
                                   , GROUP_STAT_COMMAND_OPTIONS
                                   , GROUP_GRAB_COMMAND_OPTIONS};

  options.add_options(GROUP_COMMON_OPTIONS, {
    {"i,video_filename",                      "Input file name (mandatory)", value<string>()},
    {"o,output_dir",                         "Output directory (optional)", value<string>()->default_value("output/")},
    {"t,thread_count",                       "Thread count (optional). Default 1", value<uint>()->default_value("1")},
    {"h,help", "Print help"}
  });

  options.add_options(GROUP_SEARCH_COMMAND_OPTIONS, {
    {"search",                               "Search impact"},
    {"s, search_conf_filename",               "Search impact configuration filename (optional)", value<string>()->default_value("input/conf/search.txt")},
    {"c,search_max_color_variation_allowed", "Maximum color channel (red, green or blue) variation to no detect an impact(optional)", value<uchar>()->default_value("90")},
    {"m, search_frame_mask_filename",         "Mask configuration filename (optional)", value<string>()},
  });

  options.add_options(GROUP_STAT_COMMAND_OPTIONS, {
    {"stats",                                 "Very first frame statistics"}
  });

  options.add_options(GROUP_GRAB_COMMAND_OPTIONS, {
     {"grab",                                 "Grab a frame (or a sequence of frames) by position"},
     {"p,position",                           "Position of the frame in the video. First valid frame position 0. A position can be a number, "
      "a sequence of numbers (separated by comma) or a percentage (end with character '%'). "
      "When using percentage, are included: the first frame (always), the last frame (always), and the frames located at"
      "at the percentage (regarding to total frames) value,  evenly spaced", value<string>()},
     {"k, grab_frame_mask_filename",          "Mask configuration filename (optional)", value<string>()},
   });

  //parse user inputs
  try{
    if (argc < 3){
      string help_string;
      MINILOG(logINFO) << options.help(group_list);
      show_examples();
      return false;
    }

    ParseResult result = options.parse(argc, argv);

    if (result.count("help")) {
      MINILOG(logINFO) << options.help(group_list);
      show_examples();
      return false;
    }

    if (!parse_common(result)) return false;

    if (result.count("search")) return parse_search(result);
    else
      if (result.count("stats")) return parse_stat(result);
      else
        if (result.count("grab")) return parse_grab(result);

    MINILOG(logERROR) << "Unknown argument list";
    MINILOG(logINFO) << options.help(group_list);
    show_examples();
    return false;
  }
  catch (int e){
    MINILOG(logERROR) << "An exception occurred. Exception Nr. " << e << '\n';
    return false;
  }
}
//============================================================================
bool Command_line_parser::parse_common(ParseResult& result){

  string s;
  try{

    //check mandatory arguments
    if (result.count("i") == 0){
      MINILOG(logERROR) << "The input video filename must be provided";
      return 0;
    }

    //input
    s = result["i"].as<string>();
    if(!exist_file(s)){
      MINILOG(logERROR) << "The file does not exist: '" << s << "'";
      return false;
    }
    My_video::filename = s;
    My_video::filename_no_path = get_filename(s);
    My_video::filename_no_path.erase(My_video::filename_no_path.find_last_of( '.' ) );

    //output
    s = result["o"].as<string>();
    if(exist_dir(s)){
      MINILOG(logINFO) << "Using existing directory: '" << s << "' for output";
    }
    else{
      MINILOG(logINFO) << "Creating directory: '" << s << "' for output";
      if (!make_path(s)){
	    MINILOG(logERROR) << "Error creating directory: '" << s << "' for output";
	    return false;
      }
    }
    Global_var::user_output_directoy = s + "/" + get_iso_time_string_simple_char() + "_" + My_video::filename_no_path;
    make_path(Global_var::user_output_directoy);
    MINILOG(logINFO) << "Created output directory: '" << Global_var::user_output_directoy << "'";

    //limbo ouput dir
    Global_var::user_limbo_output_dir = Global_var::user_output_directoy + "/limbo/";

    //thread count
    uint i = result["t"].as<uint>();
    Global_var::parallel_thread_count = i;
  }
  catch (int e){
	MINILOG(logERROR) << "An exception occurred. Exception Nr. " << e << '\n';
    return false;
  }

  return true;
}

//============================================================================
bool Command_line_parser::parse_search(ParseResult& result){

  bool r;
  string s;
  try{

    //conf
    s = result["s"].as<string>();
    if(!exist_file(s)){
      MINILOG(logERROR) << "The file does not exist: '" << s << "'";
      return false;
    }
    Global_var::user_search_conf_filename = s;

    Search_parser(Global_var::user_search_conf_filename, r);
    if (!r) return false;

    //color
    Global_var::user_search_frame_max_color_variation_allowed = result["c"].as<uchar>();

    //mask conf
    if (!mask_conf(result,"m")) return false;
  }
  catch (int e){
    MINILOG(logERROR) << "An exception occurred. Exception Nr. " << e << '\n';
    return false;
  }

  Global_var::user_run_search_command = true;
  return true;
}
//============================================================================
bool Command_line_parser::parse_stat(ParseResult& result){

  Global_var::user_run_stat_command = true;
  return true;
}
//============================================================================
bool Command_line_parser::parse_grab(ParseResult& result){

  string s;

  try{
    //conf
    if (result.count("p") == 0){
      MINILOG(logERROR) << "The position parameter must be provided";
      return 0;
    }
     s = result["p"].as<string>();
     trim(s);
     if (ends_with(s,"%")){  //percentage
       s.pop_back();
       trim(s);
       if (!is_number(s)){
    	  MINILOG(logERROR) << "The frame position percentage: '" << s << "' is not a valid integer";
    	  return false;
       }
       auto p = stoi(s);
       if (p < 1){
   	    MINILOG(logERROR) << "The frame position percentage: '" << p << "' must be greater than 0";
   		return false;
   	   }
       if (p > 100){
    	 MINILOG(logERROR) << "The frame position percentage: '" << p << "' must be less or equal than 100";
         return false;
       }
       Global_var::user_frame_position_percentage = p; //it will be completed late, when the frame count was known
     }
     else {
       if (s.find(",") != string::npos) { //by explicit position
         auto pos_list = split(s,",");
    	 for(auto & ps: pos_list){
    	  trim(ps);
    	  if (!is_number(ps)){
    	    MINILOG(logERROR) << "The frame position: '" << ps << "' is not a valid integer";
    	    return false;
    	   }
    	   auto p = stoll(ps);
    	   if (p < 0){
    	     MINILOG(logERROR) << "The frame position : '" << ps << "' must be greater or equal than 0";
    	     return false;
    	   }
    	   Global_var::user_frame_position_list.push_back(p);
    	   //Additional check will be done, when the frame count was known
    	 }
       }
       else { //by frame range
         auto pos_list = split(s,"-");
      	 for(auto & ps: pos_list){
      	  trim(ps);
      	  if (!is_number(ps)){
      	    MINILOG(logERROR) << "The frame position: '" << ps << "' is not a valid integer";
      	    return false;
      	   }
      	   auto p = stoll(ps);
      	   if (p < 0){
      	     MINILOG(logERROR) << "The frame position : '" << ps << "' must be greater or equal than 0";
      	     return false;
      	   }
      	   Global_var::user_frame_position_list.push_back(p);
      	   //Additional check will be done, when the frame count was known
          }
      	Global_var::user_frame_position_range = 1;
       }
     }
     //mask conf
     if (!mask_conf(result,"k")) return false;
  }
  catch (int e){
	MINILOG(logERROR) << "An exception occurred. Exception Nr. " << e << '\n';
    return false;
  }

  Global_var::user_run_grab_command  = true;
  return true;
}
//============================================================================
bool Command_line_parser::mask_conf(ParseResult& result, string shortOption){

  string s;
  bool r;

  if (result.count(shortOption) == 0){
    MINILOG(logINFO) << "Not using frame mask";
    s = "";
  }
  else{
    s = result[shortOption].as<string>();
    if(!exist_file(s)){
      MINILOG(logERROR) << "The file does not exist: '" << s << "'";
      return false;
    }
    Global_var::user_mask_conf_filename = s;
    Mask_parser(Global_var::user_mask_conf_filename, r);
    if (!r) return false;
    Global_var::user_mask_on = true;
  }
  return true;
}
//============================================================================
void Command_line_parser::show_run_parameters(void){

  MINILOG(logINFO) << "Run parameters:";
  MINILOG(logINFO) << "\tmopa version                :" << MOPA_C_VERSION;
  MINILOG(logINFO) << "\tInput video                 :" << My_video::filename;
  MINILOG(logINFO) << "\tOuput directory             :" << Global_var::user_output_directoy;

  if (Global_var::user_run_search_command) {
    MINILOG(logINFO) << "\tsearch command parameters:";

    if (Global_var::user_mask_conf_filename.empty())
      MINILOG(logINFO) << "\t\tnot using mask configuration file";
    else
      MINILOG(logINFO) << "\t\tconfiguration file          :" << Global_var::user_mask_conf_filename;

    MINILOG(logINFO) << "\t\tmax color variation allowed :" << to_string(Global_var::user_search_frame_max_color_variation_allowed);
    if (Global_var::user_mask_on){
      MINILOG(logINFO) << "\t\tMask configuration file     :" << Global_var::user_mask_conf_filename;
    }

    MINILOG(logINFO) << "\t\tThread count                :" << Global_var::parallel_thread_count;
  }
  else{
    if (Global_var::user_run_grab_command) {
      MINILOG(logINFO) << "\tgrab command parameters: ";

      if (Global_var::user_mask_conf_filename.empty())
    	  MINILOG(logINFO) << "\t\tnot using mask configuration file";
      else
    	  MINILOG(logINFO) << "\t\tconfiguration file            :" << Global_var::user_mask_conf_filename;

      if (Global_var::user_frame_position_percentage != 0)
        MINILOG(logINFO) << "\t\tgrab frame percentage       :" << Global_var::user_frame_position_percentage << "%";
      else{
    	for(auto& v : Global_var::user_frame_position_list)
    	  MINILOG(logINFO) << "\t\tgrab frame position :" << v;
      }
    }
  }
}
//=============================================================================
