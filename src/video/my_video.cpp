/*
 * mu_video.cpp
 *
 *  Created on: 12 Dec 2019
 *      Author: rafa
 */
//=============================================================================
#include "my_video.h"
//=============================================================================
extern uint x_pix_max_index;
extern uint y_pix_max_index;
//=============================================================================
My_video::My_video(){

  frame_start_index = 0;
  frame_end_index = 0;
}
//=============================================================================
My_video::My_video(bool& b, uint start_index, uint end_index){

  //init vars
  b = false;

  //open file
  MINILOG(logINFO) << "Opening video:      '" << filename << "'";
  video_capture = VideoCapture(filename);
  if (!video_capture.isOpened()){
    MINILOG(logERROR)  << "Can not open video: '" << filename << "'";
    return;
  }

  if (common_info_initialized) {
    b = true;
    frame_start_index = start_index;
    frame_end_index = end_index;
    move_to_pos(frame_start_index);
	return;
  }

  MINILOG(logINFO) << "Video back-end name:'" << video_capture.getBackendName() << "'";

  frame_total_count = video_capture.get(cv::CAP_PROP_FRAME_COUNT);
  frame_max_index = frame_total_count-1;

  frame_start_index = 0;
  frame_end_index = frame_max_index;

  double fps = video_capture.get(cv::CAP_PROP_FPS);
  uint frame_width = video_capture.get(cv::CAP_PROP_FRAME_WIDTH);
  uint frame_height = video_capture.get(cv::CAP_PROP_FRAME_HEIGHT);
  int cc = static_cast<int>(video_capture.get(cv::CAP_PROP_FOURCC));

  FrameDataType frame;
  get_frame(frame);
  move_to_pos(0);

  init_common_info(frame, fps, cc, frame_width, frame_height);

  b = true;
  common_info_initialized = true;
}
//=============================================================================
bool My_video::init_common_info(FrameDataType& frame, double fps, int cc, uint frame_width, uint frame_height) {

  MINILOG(logINFO) << "Processing video:   '" << filename << "'";

  duration_s = frame_total_count / fps;

  char cc_string[] = {(char)(cc & 0XFF),(char)((cc & 0XFF00) >> 8),(char)((cc & 0XFF0000) >> 16),(char)((cc & 0XFF000000) >> 24),0};

  Frame::slice_byte_size = sizeof(PixelComparisionDataType);
  uint x_axis_border_size = frame_width * (Frame::x_axis_border_percentage / 100);
  Frame::x_axis_min_border = x_axis_border_size;
  Frame::x_axis_max_border = frame_width - x_axis_border_size;



  uint y_axis_border_size = frame_height * (Frame::y_axis_border_percentage / 100);
  Frame::y_axis_min_border = y_axis_border_size;
  Frame::y_axis_max_border = frame_height - y_axis_border_size;

  MINILOG(logINFO) << "\tVideo duration          :" << duration_s << " s";
  MINILOG(logINFO) << "\tFrame count             :" << frame_total_count << " frames";
  MINILOG(logINFO) << "\tFrame width             :" << frame_width << " pix";
  MINILOG(logINFO) << "\tFrame height            :" << frame_height << " pix";
  MINILOG(logINFO) << "\tFrame rate              :" << fps << " frames/s";
  MINILOG(logINFO) << "\t4-character codec       :" << cc_string;
  MINILOG(logINFO) << "\tx axis border size      :" << x_axis_border_size << " pixels";
  MINILOG(logINFO) << "\ty yxis border size      :" << y_axis_border_size << " pixels";
  MINILOG(logINFO) << "\tFrame slice size        :" << Frame::slice_byte_size << " bytes";

  Frame::pix_byte_size = frame.elemSize();
  Frame::pix_count = frame.total();

  Frame::x_axis_pix_count = frame_width;
  Frame::y_axis_pix_count = frame_height;
  Frame::byte_size = Frame::pix_count * Frame::pix_byte_size;
  Frame::slice_count = Frame::byte_size / Frame::slice_byte_size;

  //set useful data
  Frame::x_pix_max_index = Frame::x_axis_pix_count - 1;
  Frame::y_pix_max_index = Frame::y_axis_pix_count - 1;

  x_pix_max_index =  Frame::x_pix_max_index;
  y_pix_max_index =  Frame::y_pix_max_index;

  MINILOG(logINFO) << "\tFrame pix byte size     :" << Frame::pix_byte_size << " bytes";
  MINILOG(logINFO) << "\tFrame pix count         :" << Frame::pix_count << " pixels";
  MINILOG(logINFO) << "\tFrame byte size         :" << Frame::byte_size << " bytes";
  MINILOG(logINFO) << "\tFrame slice count       :" << Frame::slice_count << " slices";

  if ((Frame::byte_size % Frame::slice_byte_size) > 0){
    MINILOG(logWARNING) << "The frame byte size:" << Frame::byte_size << "is not a multiplier of 8, last slice will not processed properly";
  }
  get_date();

  return true;
}

//-------------------------------------------------------------------------
bool My_video::get_frame(FrameDataType& frame) {

  video_capture >> frame;

  //check frame
  if(frame.empty()) {
    MINILOG(logERROR) << "Error getting a frame";
    return false;
  }
  return true;
}
//-------------------------------------------------------------------------
bool My_video::get_frame(FrameDataType& frame, uint frame_index) {

  if(!move_to_pos(frame_index)) return false;
  return get_frame(frame);
}
//-------------------------------------------------------------------------
bool My_video::move_to_pos(uint pos){

  if (!video_capture.set(cv::CAP_PROP_POS_FRAMES, pos)){
    MINILOG(logERROR) << "Error setting the frame position: " << pos;
	return false;
  }

  uint read_pos = (uint) video_capture.get(cv::CAP_PROP_POS_FRAMES);
  if (pos != read_pos) {
    MINILOG(logERROR) << "Error setting the frame position: " << pos;
    return false;
  }
  return true;
}
//-------------------------------------------------------------------------
uint My_video::get_pos(void){

  return (uint) video_capture.get(cv::CAP_PROP_POS_FRAMES);
}
//-------------------------------------------------------------------------
void My_video::get_read_fps(void){

  MINILOG(logINFO) << "Calculating the frame per second reading performances";
  auto start_time = chrono::steady_clock::now();

  for (uint i = frame_start_index; i <=  frame_end_index ; ++i) {
    FrameDataType frame;
    if(!get_frame(frame)){
      MINILOG(logINFO) << "Error reading frame: " << i;
      return;
    }
  }

  auto processing_time = chrono::duration_cast<chrono::seconds> (chrono::steady_clock::now() - start_time);
  MINILOG(logINFO) << "Elapsed time: " << processing_time.count() << "s";
  MINILOG(logINFO) << "Frame per second: " <<  frame_total_count / processing_time.count() << "fps";
}
//-------------------------------------------------------------------------
void My_video::get_date(void) {  //using external program mediainfo

  string command = "mediainfo " +  filename + " | grep 'Encoded date' | head -n 1";
  string r = system_command(command.c_str());
  trim(r);
  auto split_r = split(r, string(": UTC "));

  if (r.size() == 0) {
    command = "mediainfo " +  filename + " | grep 'Recorded date' | head -n 1";
	r = system_command(command.c_str());
	trim(r);
	split_r = split(r, string(" : "));
  }
  else {
	if (split_r.size() != 2){
	MINILOG(logWARNING) << "Timestamp found in the video, but unknown format: '" << r << "'.Using as time reference the second 0 of the video";
	return;
    }
  }

  if (r.size() == 0){
    MINILOG(logWARNING) << "Can not find the time stamp in the video information" << ".Using as time reference the second 0 of the video";
   return;
  }

  string date_string = split_r[1];
  trim(date_string);
  tm tm = {};
  tm.tm_sec = -1; //set a flag when it was properly parsed

  stringstream ss(date_string);
  ss >> get_time(&tm, "%Y-%m-%d %H:%M:%S"); //time format used by mediainfo
  if (ss.fail()) {
    MINILOG(logWARNING) << "Timestamp found in the video, but error parsing: '" << date_string << "'.Using as time reference the second 0 of the video";
    return;
  }
  else {
    MINILOG(logINFO) << "Video timestamp: " << date_string;
    has_timestamp = true;
    start_time_stamp = chrono::system_clock::from_time_t( mktime(&tm) );
    return;
  }
}
//=============================================================================
