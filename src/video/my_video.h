/*
 * video.h
 *
 *  Created on: Dec 1, 2019
 *      Author: rafa
 */
//=============================================================================
#ifndef VIDEO_MY_VIDEO_H_
#define VIDEO_MY_VIDEO_H_
//=============================================================================
//-----------------------------------------------------------------------------
#include <ctime>
#include <chrono>
#include <thread>
//-----------------------------------------------------------------------------
#include "global_var.h"
#include "global_var.h"
#include "logger/minilog.h"
#include "util/util.h"
#include "frame/frame.h"
#include "concurrent_storage/concurrent_vector.hpp"
//=============================================================================
class My_video
{
  //---------------------------------------------------------------------------
  public:
    //-------------------------------------------------------------------------
    //class variables (common to all objects oh this class)
    static inline string filename;
    static inline string filename_no_path;
    static inline float   duration_s = 0;
    static inline uint   frame_total_count = 0;
    static inline uint   frame_max_index = 0;
    static inline bool   has_timestamp = false;
    static inline chrono::system_clock::time_point start_time_stamp;
    //-------------------------------------------------------------------------
    My_video();
    //-------------------------------------------------------------------------
    My_video(bool& b, uint start_index=0, uint end_index=0);
    //-------------------------------------------------------------------------
    virtual ~My_video (){video_capture.release();};
    //-------------------------------------------------------------------------
    static bool init_common_info(FrameDataType& frame, double fps, int cc, uint frame_width, uint frame_height);
    //-------------------------------------------------------------------------
    bool get_frame(FrameDataType& frame);
    //-------------------------------------------------------------------------
    bool get_frame(FrameDataType& frame, uint frame_index);
    //-------------------------------------------------------------------------
    bool move_to_pos(uint pos);
    //-------------------------------------------------------------------------
    uint get_pos(void);
    //-------------------------------------------------------------------------
    void get_read_fps(void);
    //-------------------------------------------------------------------------
    uint get_frame_start_index(void) {return frame_start_index;}
    //-------------------------------------------------------------------------
    uint get_frame_end_index(void) {return frame_end_index;}
    //-------------------------------------------------------------------------
    uint get_relative_time_stamp_ms(void) {return video_capture.get(cv::CAP_PROP_POS_MSEC);}
    //-------------------------------------------------------------------------
  private:
    //object variables (specific for each object of this class)
    VideoCapture video_capture;
    uint frame_start_index;  //last indes is take into account
    uint frame_end_index;

    static inline bool common_info_initialized = false;
    //-------------------------------------------------------------------------
    static void get_date(void);
    //-------------------------------------------------------------------------
    static void get_and_show_info(void);
    //-------------------------------------------------------------------------
};
//=============================================================================
#endif /* VIDEO_MY_VIDEO_H_ */
