#------------------------------------------------------------------------------
# mopa

MOon imPact Analyzer (mopa) version in C++ 17 language for gnu-linux computers
basen on opencv (https://opencv.org/)

#------------------------------------------------------------------------------
#use

mopa has been designed to perform an automatic detection of impacts (flashes) on
moon's video files. It is input is an video file (view requirements), the configuration file
and the program parameters. The very first time, the configuration file must be instantiated
using the template provided: just change the file name from 'input/conf/search_template.txt' to 'search.txt'

You can obtain a help on mopa, just typing "mopa_c". Some use case are:

    Example 01 : Calculate statistics                                              : mopa_c -i /my_video.avi --stats 
    Example 02 : Search impact with no mask                                        : mopa_c -i /my_video.avi --search 
    Example 03 : Search impact with no mask using 4 threads                        : mopa_c -i /my_video.avi --search -t 4
    Example 04 : Search impact with no mask and color variation 60                 : mopa_c -i /my_video.avi --search -c 60
    Example 04 : Search impact with no mask using 4 threads and color variation 60 : mopa_c -i /my_video.avi --search -t 4 -c 60
    Example 05 : Search impact with fixed mask                                     : mopa_c -i /my_video.avi --search -m /my_mask.conf
    Example 06 : Search impact with mask and trajectory                            : mopa_c -i /my_video.avi --search -m /my_mask.conf
    Example 07 : Grab the frame 1                                                  : mopa_c -i /my_video.avi --grab -p 1
    Example 08 : Grab the frames 1 and 5                                           : mopa_c -i /my_video.avi --grab -p 1,5
    Example 09 : Grab 4 frames at 25% position                                     : mopa_c -i /my_video.avi --grab -p 25%
    Example 10 : Grab 4 frames at 25% position with trajectory                     : mopa_c -i /my_video.avi --grab -p 25% -k /my_mask.conf'
    Note: When the mask configuration file had a trajectory, it will be used automatically (examples 4 and 8)


The output of mopa is:

  i) For each impact: video frame previous to the impact, and the frame of the impact
  ii) "comma separated value" csv file summarizing all impacts detected

#------------------------------------------------------------------------------
#requirements

0) gnu-linux operating system

1) video format, codec and decodec supported by opencv

2) video frame must have 3 byte per pixel.

3) mopa is highly optimized for video frame subtraction using aggressive optimizations
described in https://www.agner.org/optimize/ ,including "video class library"
instantiated for avx2 cpu registers. So, the cpu of the computer that runs mopa, 
must have a cpu that supports avx2. To check if your computer is compatible 
with mopa, run the following command in the console:

    lscpu | grep avx2

In none output is obtained, then your computer is not compatible.

#------------------------------------------------------------------------------
#dependences

mopa has embedded the opencv libraries, but typically additional libraries are required

    #Debian/Ubuntu
    sudo apt install libopenblas-dev libconfig++-dev libwebp-dev mediainfo


    #Centos/Fedora
    sudo yum install openblas-devel libwebp-devel mediainfo
#------------------------------------------------------------------------------
#capabilities

i)    Automatic detection of moon impacts (flashes) on video files

ii)   Fixed mask definition

iii)  Moving mask trayectory definition  

iv)   Very first frame statistics: average, min, max and standard deviation

v)    Frame grab

vi)   Automatic detection of brilliant zones (blobs)
#------------------------------------------------------------------------------
